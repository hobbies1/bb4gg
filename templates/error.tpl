<apply template="/base">
  <bind tag="page-title">
    Error
  </bind>
  <bind tag="page-headers">
    <if var="redirect">
      <meta http-equiv="refresh" content="5;${redirect-location}" />
    </if>
  </bind>
  <bind tag="page-body">
    <apply template="/error-span" />
  </bind>
</apply>
