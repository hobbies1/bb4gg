<apply template="/base">
  <bind tag="page-title">
    Instances
  </bind>
  <bind tag="page-headers">
  </bind>
  <bind tag="page-body">
    <h1>Instances</h1>
    <table>
      <tbody>
        <instances>
          <tr style="vertical-align: text-bottom">
            <td>
              <a href="/${name}/"><b style="font-size: x-large"><name /></b></a>
            </td>
          </tr>
        </instances>
      </tbody>
    </table>
    <p></p>
    <dfForm action="/" autocomplete="off">
      <dfInputText ref="name" minlength="1" maxlength="${max-instance-name-length}" autofocus style="width: calc(${max-instance-name-length} * 1.2ch);" />
      <errors ref="name" />
      <br />
      <input type="submit" value="Create new instance" class="button confirm" />
    </dfForm>
  </bind>
</apply>
