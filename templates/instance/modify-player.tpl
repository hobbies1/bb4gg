<apply template="/instance/base">
  <bind tag="instance-page-title">
    <if var ="modify">
      Modify Player
      <else />
      Add Player
    </if>
  </bind>
  <bind tag="instance-page-headers">
    <style>
      .action-row {
        display: flex;
        align-items: center;
        width: max-content;
      }

      .action-row > *+* {
        padding-left: 1em;
      }
      #statuses label {
        padding-right: 0.5em;
      }
      
      .specialGameName {
        font-style: italic;
      }
      .activity-True {
        font-weight: bold;
      }
      td {
        white-space: nowrap;
      }
      
      .game-info-cell {
        display: flex;
        justify-content: space-between;
        column-gap: 0.7em;
      }
      
      .preferenceInput, .maxPlayerNumberInput, .minPlayerNumberInput {
        width: 3em;
      }
      
      #submitReady {
        margin-right: 1em;
      }
    </style>
    <meta name="init-is-preferred" content="${init-is-preferred}" />
    <meta name="max-preference" content="${max-preference}" />
    <meta name="player-id" content="${player-id}" />
    <meta name="player-name" content="${player-name}" />
    <if var="modify">
      <script src="/_static/confirm.js" />
    </if>
    <script type="module"> 
        import {injectedVariables} from '/_static/inject_variables.js';

        const playerId = injectedVariables['player-id'];

        function getCheckbox() {
            return document.getElementById('check-me-input');
        }

        function updateCheckbox() {
            const checkbox = getCheckbox();
            if(checkbox) {
              getCheckbox().checked = getPlayerId() === playerId;
            }
        }

        // This is NOT showOnPlayerExists, but only invisible if browser does not support localStorage.
        if (playerId !== '' && getPlayerId() !== undefined) {
            if(injectedVariables['player-name'] === '' && getPlayerId() == playerId) {
                // This only occurs for deleted players.
                removePlayerId();
                window.location = '/' + injectedVariables['instance-name'];
            }
            document.getElementById('check-me-div').style.display = '';
            getCheckbox().addEventListener('click', () => togglePlayerId(playerId));
            updateCheckbox();
        }

        onIdChange(updateCheckbox);
        document.getElementById(`delete-player-${playerId}`)?.addEventListener('submit', () => {
            if(playerId === getPlayerId()) {
                removePlayerId();
            }
        });
    </script>
    <link rel="stylesheet" type="text/css" href="/_static/bgg-tooltip.css" />
    <script src="/_static/bgg-tooltip.js" defer />
  </bind>
  <bind tag="instance-page-body">
    <if var="modify">
      <h2>Modify a Player</h2>
      <div class="action-row">
        <form id="delete-player-${player-id}" action="/${instance-name}/player/modify/delete" method="post">
          <input type="hidden" name="id" value="${player-id}" />
          <input type="button" onclick="confirmDeletion('player', '${player-name}', ${player-id})" value="Delete this player (${player-name})" class="button delete" />
        </form>

        <div id="check-me-div">
          <input type="checkbox" id="check-me-input"> <label for="check-me-input">That's me!</label>
      </div>
      </div>
      <else />
      <h2>Add a Player</h2>
    </if>

    <dfForm action="/${instance-name}/player/modify/?id=${player-id}">
      <dfIfChildErrors ref="">
        <p class="error">
          Your input contained some errors, please check it again.
        </p>
      </dfIfChildErrors>

      <p>
        <dfLabel ref="name"><strong>Name: </strong></dfLabel>
        <dfInputText ref="name" minlength="1" maxlength="${max-player-name-length}" autocomplete="nickname" if-then="new=>autofocus" style="width: calc(${max-player-name-length} * 1.2ch);" />
        <errors ref="name" />
      </p>

      <p id="statuses">
        <strong>Status<if var="inGame"> (after game)</if>:</strong>
        <dfInputRadio ref="status" />
        <errors ref="status" />
      </p>

      <if var="locked">
        <p class="error">
          Warning: The results are locked until
          <apply template="/instance/locked-time" />.
          Changes in preferences won't have any effect on the results until then.
        </p>
      </if>

      <p>
        You can enter integral preferences from 1 (lowest) to <max-preference /> (highest) for the games you want to play.
        You can also enter another range for the number of players with which you would want to play a game.
        You can use the checkboxes (or clicking on a name) to (de)activate a game preference.
        Activating a game for the first time will set its preference to 10.
        Currently not available games are greyed out.
      </p>
      <p class="extra">
        In the "Extra" field of a game you can enter extra preferences that add to or overwrite your single preference for (ranges of) certain numbers of players.
        The entered string needs to consist of space separated blocks of the form "range,range,...,range:score" where "range" is either a (natural) number or "number-number".
        Using "0" as a score means you don't want to play the game with the specified player numbers.
        Example: "1-2,5:3 3,7-8,10-11:0 4:1"
      </p>
      <p>
        <button type="button" onclick="toggleExtra()" class="button" >Toggle advanced options</button>
      </p>

      <errors ref="" />

      <div>
        <table>
          <thead>
          <tr>
              <th>✓</th>
              <th>Pref</th>
              <th>Game</th>
              <th>Min</th>
              <th>Max</th>
              <th><span class="extra">Extra</span></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr class="activity-True">
              <td><input type="checkbox" checked disabled /></td>
              <td><dfInput ref="game-NotPlayingSG.pref" type="number" min="${not-playing-min-preference}" max="${not-playing-max-preference}" class="preferenceInput" /></td>
              <td><span class="specialGameName">Not playing</span></td>
              <td></td>
              <td></td>
              <td></td>
              <td><child-errors ref="game-NotPlayingSG" /></td>
            </tr>
            <normal-games>
              <tr id="game-row-${game-id}" class="availability-${game-availability} activity-${game-activity}">
                <td>
                  <dfInputCheckbox ref="game-${game-id}.active" onclick="checkboxClicked(${game-id})" />
                </td>
                <td>
                  <dfInput ref="game-${game-id}.pref" type="number" min="0" max="${max-preference}" class="preferenceInput" onchange="prefChanged(${game-id})" />
                </td>
                <td class="game-info-cell">
                  <dfLabel ref="game-${game-id}.active">
                    <apply template="/instance/game-info/normal" />
                  </dfLabel>
                  <apply template="/instance/game-info/more-info" />
                </td>
                <td>
                  <dfInput ref="game-${game-id}.minP" type="number" min="${game-min}" max="${game-max}" class="minPlayerNumberInput" />
                </td>
                <td>
                  <dfInput ref="game-${game-id}.maxP" type="number" min="${game-min}" max="${game-max}" class="maxPlayerNumberInput" />
                </td>
                <td class="extra">
                  <dfInputText ref="game-${game-id}.extraPrefs" class="extraPreferencesInput" onchange="prefChanged(${game-id})" />
                </td>
                <td>
                  <child-errors ref="game-${game-id}" />
                  <span hidden class="recommendedMin" data-number="${game-recommended-min}" />
                  <span hidden class="recommendedMax" data-number="${game-recommended-max}" />
                </td>
              </tr>
            </normal-games>
          </tbody>
        </table>
      </div>

      <p>
        <if var="requireReady">
          <if var="inGame">
            <dfInput ref="submitReady" type="submit" value="Submit &amp; Ready (after game)" id="submitReady" class="button confirm" />
          <else />
            <dfInput ref="submitReady" type="submit" value="Submit &amp; Ready" id="submitReady" class="button confirm" />
          </if>
          <input type="submit" value="Just Submit" id="justSubmit" class="button confirm" />
        <else />
          <input type="submit" value="Submit" class="button confirm" />
        </if>
      </p>
    </dfForm>

    <script>
      window.isPreferred = JSON.parse(
        document.querySelector('meta[name="init-is-preferred"]').content
      );

      function prefChanged(gameID) {
        var els = getGameData(gameID);
        var isNowPreferred =
          els.pref.value.trim() != '' || els.extra.value.trim() != '';
        if (isNowPreferred && !window.isPreferred[gameID]) {
          els.checkbox.checked = true;
        } else if (!isNowPreferred) {
          els.checkbox.checked = false;
        }
        refreshActivity(els);
        window.isPreferred[gameID] = isNowPreferred;
      };

      function checkboxClicked(gameID) {
        var data = getGameData(gameID);
        if (data.checkbox.checked) {
          if (data.pref.value.trim() == '' && data.extra.value.trim() == '') {
            data.pref.value =
              document.querySelector('meta[name="max-preference"]').content;
            if (data.minPlayers.value == '') {
              data.minPlayers.value = data.recommendedMin
            }
            if (data.maxPlayers.value == '') {
              data.maxPlayers.value = data.recommendedMax
            }
            window.isPreferred[gameID] = true;
          }
        }
        refreshActivity(data);
      };

      function refreshActivity(els) {
        els.row.classList.toggle('activity-True', els.checkbox.checked);
        els.row.classList.toggle('activity-False', !els.checkbox.checked);
      };

      function getGameData(gameID) {
        var row = document.getElementById('game-row-' + gameID);
        return {
          row: row,
          checkbox: row.querySelector('input[type=checkbox]'),
          pref: row.querySelector('.preferenceInput'),
          maxPlayers: row.querySelector('.maxPlayerNumberInput'),
          minPlayers: row.querySelector('.minPlayerNumberInput'),
          extra: row.querySelector('.extraPreferencesInput'),
          recommendedMin:
            row.querySelector('.recommendedMin').getAttribute('data-number'),
          recommendedMax:
            row.querySelector('.recommendedMax').getAttribute('data-number')
        };
      }

      function toggleExtra() {
        document.querySelectorAll('.extra').forEach(el => {
          if (el.style.display === 'none') {
            el.style.display = 'initial';
          } else {
            el.style.display = 'none';
          }
        });
      };
    </script>
    <if var="requireReady">
      <script>
        function statusChanged(status) {
          var justSubmitButton = document.getElementById('justSubmit');
          var submitReadyButton = document.getElementById('submitReady');
          if (status === 'ready') {
            justSubmitButton.value = "Submit";
            submitReadyButton.style.display = "none";
          } else {
            justSubmitButton.value = "Just Submit";
            submitReadyButton.style.display = "initial";
          }
        };

        document.querySelectorAll('#statuses input').forEach(el => {
          el.addEventListener(
            'input',
            function (ev) {statusChanged(ev.target.value);}
          );
        });

        statusChanged(document.querySelector('#statuses input:checked').value);
      </script>
    </if>
    <if var="locked">
      <script>
        var el = document.getElementById('lockedTime');
        var d = new Date(parseInt(el.getAttribute('data-time')) * 1000);
        el.innerHTML = d.toTimeString().slice(0, 8);
      </script>
    </if>
    <if var="noExtraPrefs">
      <script>
        document.querySelectorAll('.extra').forEach(el => {
          el.style.display = 'none';
        });
      </script>
    </if>
  </bind>
</apply>
