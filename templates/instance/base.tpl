<apply template="/base">
  <bind tag="page-title">
    <instance-page-title /> | <instance-name />
  </bind>
  <bind tag="page-headers">
    <script src="/_static/local_storage.js" />
    <style>
      header a {padding: 0.5em; white-space: nowrap;}
    </style>
    <style id="player-id-style">
      span[data-player-id="NOPE"] {
        font-weight: bold;
      }
      // Do not add any style here, this will be changed by JavaScript.
      // Also do not add a comment above the rule, since it will somehow break the StyleSheetElement.
    </style>
    <meta name="instance-name" content="${instance-name}" />
    <script>
        const editMeId = 'edit-me-anchor';
        const getRule = () => document.getElementById('player-id-style').sheet.rules[0];
        const selectorRegex = /"(NOPE|\d+)"/;

        function updateEditMe(newId) {
            const editMe = document.getElementById(editMeId);
            if(editMe) {
                editMe.style.display = (newId === null) ? 'none' : '';
                editMe.href = `/${instanceName}/player/modify/?id=${newId}`;
            }
        }

        function updateStyle(newId) {
            const newSelector = getRule().selectorText.replace(selectorRegex, `"${newId}"`);
            getRule().selectorText = newSelector;
        }

        onIdChange(updateEditMe);
        onIdChange(updateStyle);
    </script>
    <instance-page-headers />
  </bind>
  <bind tag="page-body">
    <header>
      <a href="/${instance-name}/">Main</a>
      <a href="/${instance-name}/results/">Results</a>
      <a href="/${instance-name}/player/modify/">Add Player</a>
      <a id="edit-me-anchor" href="#">Edit Me</a>
      <a href="/${instance-name}/games/modify/">Modify Games</a>
      <a href="/${instance-name}/configure/">Configure Instance</a>
      <hr />
    </header>
    <instance-page-body />
  </bind>
</apply>
