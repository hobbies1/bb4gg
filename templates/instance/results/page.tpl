<apply template="/instance/base">
  <bind tag="instance-page-title">
    Results
  </bind>
  <bind tag="instance-page-headers">
    <style>
      .choose-assignment-form {display: inline}
      .game-name {font-weight: bold}
      .specialGameName {font-style: italic}
    </style>
    <script type="module">
      import { setUpWebSockets } from '/_static/websockets.js';
      import { injectedVariables } from '/_static/inject_variables.js';

      function updateTime() {
       const el = document.getElementById('lockedTime');
       if(el) {
          const d = new Date(parseInt(el.getAttribute('data-time')) * 1000);
          el.innerHTML = d.toTimeString().slice(0, 8);
       }
      }

      window.addEventListener('load', () => {
        updateTime();
        setUpWebSockets('ResultsChannel', (event) => {
          if(event.data === 'ASSIGNMENT CHOSEN') {
            event.target.close();
            document.getElementById('results').innerHTML =
              'An assignment has been chosen. Redirecting to main page...';
            window.location.href = '/' + injectedVariables['instance-name'] + '/';
          } else {
            document.getElementById('results').innerHTML = event.data;
            updateTime();
          }
        });
      });
    </script>
  </bind>
  <bind tag="instance-page-body">
    <apply template="/instance/disconnected" />
    <div id="results">
      <apply template="/instance/results/content" />
    </div>
  </bind>
</apply>
