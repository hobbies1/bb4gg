<apply template="/instance/base">
  <bind tag="instance-page-title">
    Main
  </bind>
  <bind tag="instance-page-headers">
    <style>
      .dismiss-game {display: inline}
      .dismiss-game .button {margin-bottom: 4px}
      #players-lines-table th {
        text-align: right;
        vertical-align: text-top;
        white-space: nowrap;
      }
      .players-line-title {
        display: inline-block;
        margin: 1px 0 4px 0;
      }
      .player-with-arrows {
        display: inline-block;
        border: 1px solid var(--border-color);
        border-radius: 4px;
        padding: 0 3px;
        margin: 0 0.05em 3px 0.05em;
        white-space: nowrap;
      }
      .player-status-form {display: inline}
      .arrow-button {
        padding: 0;
        border: none;
        background: none;
      }
      .disabled {
        filter: grayscale(100%);
      }
      .up-arrow:hover {filter: brightness(150%)}
      .down-arrow:hover, .double-down-arrow:hover {filter: brightness(70%)}
      .game-name {font-weight: bold}
      .specialGameName {font-style: italic}
      #preferencesTable .ready {text-decoration: underline}
    </style>
    <script type="module">
      import { setSubmitListeners } from '/_static/background_form.js';
      import { setUpWebSockets } from '/_static/websockets.js';
      // Needs to be done every time the contents are reset.
      setSubmitListeners();

      window.addEventListener('load', () => setUpWebSockets('MainChannel', (event) => {document.getElementById('mainContent').innerHTML = event.data; setSubmitListeners()}));
    </script>
  </bind>
  <bind tag="instance-page-body">
    <apply template="/instance/disconnected" />
    <div id="mainContent">
      <apply template="/instance/main/content" />
    </div>
  </bind>
</apply>
