<h2>Current Assignment</h2>
<if var="gamesInProgress">
  <assignment-games>
    <game-info />:
    <ul class="comma-list">
      <game-playing-players>
        <li><apply template="player-span" /></li>
      </game-playing-players>
    </ul>
    -
    <form action="/${instance-name}/results/dismiss" method="post" class="dismiss-game">
      <input type="hidden" name="id" value="${assignment-game-id}" />
      <input type="submit" value="Dismiss this game" class="button inline delete" />
    </form>
    <br />
  </assignment-games>
  <else />
  N/A
</if>

<h2>Players</h2>
<p>
  Click on a player's name to modify their preferences. Click on an arrow to change the status of that player.
</p>
<table id="players-lines-table">
  <if var="gamesInProgress">
    <players-line status="Playing" />
  </if>
  <if var="requireReady">
    <players-line status="ready" />
  </if>
  <players-line status="wantsToPlay" />
  <players-line status="inactive" />
</table>

<preferences>
  <h2>Preferences</h2>
  <p>
    The score is the sum of all preferences for that game; the value in parantheses is the average among positive ones.
    <if var="requireReady">
      An underlined name means that the player is marked as ready.
    </if>
  </p>
  <table id="preferencesTable">
    <thead>
      <tr>
        <th colspan="2">Score (Avg)</th>
        <th>Game</th>
        <th>Active Players</th>
      </tr>
      <tr>
        <td colspan="4" style="border-bottom: 1px solid black">
        </td>
      </tr>
    </thead>
    <tbody>
      <game-row>
        <tr class="availability-${game-availability}">
          <td style="text-align: right"><score-sum /></td>
          <td style="text-align: left">(⌀ <average-score />)</td>
          <td style="white-space: nowrap; text-align: center">
            <apply template="/instance/game-info/normal" />
          </td>
          <td>
            <ul class="comma-list">
              <preferring-players>
                <li><span class="${player-status}"><apply template="player-span" /></span></li>
              </preferring-players>
            </ul>
          </td>
        </tr>
      </game-row>
    </tbody>
  </table>
</preferences>
