<apply template="/instance/base">
  <bind tag="instance-page-title">
    Configure Instance
  </bind>
  <bind tag="instance-page-headers">
    <style>
      #update-with-bgg {
        display: flex;
        flex-direction: row;
        align-items: center;
      }
      #update-with-bgg .label {
        margin-right: 1ch;
      }
      #update-with-bgg .button {
        margin-left: 50px;
      }
    </style>
    <script src="/_static/confirm.js" />
  </bind>
  <bind tag="instance-page-body">
    <dfForm action=".">
      <h2>Configure this Instance</h2>
      <p>
        <dfInputCheckbox ref="listed" />
        <dfLabel ref="listed">
          Make this instance appear in the public list of instances.
        </dfLabel>
      </p>
      <p>
        Compute up to
        <dfInputText type="number" ref="assignments-to-compute" min="1" style="width: 3em" />
        best assignments.
        <errors ref="assignments-to-compute" />
      </p>
      <p>
        <dfInputCheckbox ref="require-ready-bool" onchange="requireReadyChanged(this)" />
        <dfLabel ref="require-ready-bool">
          Require all players to mark themselves as ready before displaying possible assignments.
        </dfLabel>
        Lock the results for
        <dfInputText type="number" ref="require-ready-duration" min="0" style="width: 3em" onchange="lockDurationChanged(this)" />
        seconds once they are displayed.
        <errors ref="require-ready-duration" />
      </p>
      <p>
        Deactivate inactive players and assignments after
        <dfInputText type="number" ref="auto-deactivation-hours" min="1" style="width: 3em" />
        hours (leave empty for never).
        <errors ref="auto-deactivation-hours" />
      </p>
      <p>
        <input type="submit" value="Save" class="button confirm" />
      </p>
      <script>
        function requireReadyChanged(e) {
          var durationInput = document.getElementById('configureInstanceForm.require-ready-duration');
          if (e.checked) {
            durationInput.value = 0;
          } else {
            durationInput.value = '';
          }
        }
      </script>
      <script>
        function lockDurationChanged(e) {
          var boolInput = document.getElementById('configureInstanceForm.require-ready-bool');
          if (e.value >= 0) {
            boolInput.checked = true;
          } else if (0 > e.value) {
            e.value = boolInput.checked ? '0' : '';
          }
        }
      </script>
    </dfForm>
    <hr />
    <p>
      Automatically link games with their <a target="_blank" rel="noopener noreferrer" href="https://boardgamegeek.com/">BoardGameGeek</a> counterpart. This also imports recommended player numbers if they are not already set (and might take a few minutes).
    </p>
    <form id="update-with-bgg" action="/${instance-name}/update-with-bgg" method="post">
      <span class="label">Game types to look for (select at least one):</span>
      <select name="type" multiple required size="2">
        <option value="boardgame boardgameexpansion">Boardgames</option>
        <option value="videogame">Videogames</option>
      </select>
      <input type="submit" class="button confirm" value="Link games with BGG" />
    </form>
    <hr />
    <form id="delete-instance-${instance-name}" action="/_instances/delete" method="post">
      <input type="hidden" name="id" value="${instance-name}" />
      <input type="button" onclick="confirmDeletion('instance', '${instance-name}', '${instance-name}')" value="Delete this instance (${instance-name})" class="button delete" />
    </form>
  </bind>
</apply>
