<apply template="/instance/base">
  <bind tag="instance-page-title">
    Error
  </bind>
  <bind tag="instance-page-headers">
    <if var="redirect">
      <meta http-equiv="refresh" content="5;${redirect-location}" />
    </if>
  </bind>
  <bind tag="instance-page-body">
    <apply template="/error-span" />
  </bind>
</apply>
