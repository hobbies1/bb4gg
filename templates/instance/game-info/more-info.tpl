<game-bgg-id>
  <details class="game-more-info">
    <summary><span class="info-glyph">&#8505;</span></summary>
    <div class="game-bgg-tooltip">
      <iframe class="game-bgg-tooltip-html" src="about:blank" data-src="/_bgg?id=${value}" title="BoardGameGeek data for ${game-name}" />
    </div>
  </details>
</game-bgg-id>
