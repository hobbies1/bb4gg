function confirmDeletion (type, name, id) {
  const message = `Are you sure you want to delete the ${type} "${name}"?`;
  if (window.confirm(message)) {
    document.getElementById(`delete-${type}-${id}`).requestSubmit();
  }
}
