document.querySelectorAll('.game-more-info').forEach(el => {
  const tooltipDiv = el.querySelector('.game-bgg-tooltip');
  
  if(tooltipDiv) {
    const tooltipHTML = tooltipDiv.querySelector('.game-bgg-tooltip-html');
    
    // Adapt the size of the tooltip to its content
    const setSize = () => {
        const contentHeight = tooltipHTML.contentWindow.document.documentElement.scrollHeight;
        if(contentHeight > 0) {
          tooltipDiv.style.setProperty('--content-height', `${contentHeight}px`);
          tooltipHTML.style.visibility = "visible";
          document.body.style.setProperty('--bgg-tooltip-wrap-height', `${tooltipDiv.scrollHeight}px`);
        } else {
          // Sometimes the "load" event fires before the content has rendered and its height is known
          setTimeout(setSize, 100);
        }
    }
    
    tooltipHTML.addEventListener('load', e => {
      if(e.target.getAttribute('src') != "about:blank") {
        setSize();
      }
    });
    
    // Triggers when the tooltip is opened or closed
    el.addEventListener('toggle', e => {
      if(el.open) {
        if(tooltipHTML.getAttribute('src') == "about:blank") {
          tooltipHTML.setAttribute('src', tooltipHTML.getAttribute('data-src'));
        } else {
          // Has to run after the event completed.
          setTimeout(setSize, 1);
        }
      } else {
        document.body.style.removeProperty('--bgg-tooltip-wrap-height');
      }
    });
  }
});

// Close all tooltips if somewhere else was clicked
var tooltips = [...document.querySelectorAll('.game-more-info')];
document.addEventListener('click', e => {
  tooltips.forEach(t => {
    if(!t.contains(e.target)) {
      t.removeAttribute('open');
    } 
  });
})
