// Immediately invoked function to avoid cluttering global namespace.
const instanceName = (() => {
    const pathInstanceRegex = /^\/([^\/]*)\//;
    const matched = window.location.pathname.match(pathInstanceRegex);
    console.assert(matched);
    return matched[1];
}) ();
const playerKey = `${instanceName}.playerId`;

const idChangeHandler = [];

function onIdChange(func) {
    // We often need the same behavior as soon as possible.
    // Run it instantly and again on load for things that weren't ready, yet.
    const run = () => {const playerId = getPlayerId(); func(playerId, playerId)};
    run();
    window.addEventListener('DOMContentLoaded', run);

    // The storage event is only fired in other windows/tabs, so we need to implement our own for this page.
    idChangeHandler.push(func);

    window.addEventListener('storage', (event) => {
        if(event.key === playerKey) {
            func(event.newValue, event.oldValue);
        }
    });
}

function fireEvent(oldValue=null) {
    for(const f of idChangeHandler) {
        f(getPlayerId(), oldValue);
    }
}

// Returns null if if id is not stored and undefined if localStorage is unavailable.
function getPlayerId() {
    try {
        return localStorage.getItem(playerKey);
    } catch(e) {
        console.log(e);
        return undefined;
    }
}

function setPlayerId(id) {
    try {
        const oldId = getPlayerId();
        localStorage.setItem(playerKey, id);
        fireEvent(oldId);
        return true;
    } catch(e) {
        return false;
    }
}

function removePlayerId() {
    try {
        const oldId = getPlayerId();
        localStorage.removeItem(playerKey);
        fireEvent(oldId);
        return true;
    } catch(e) {
        return false;
    }
}

function togglePlayerId(id) {
    if(id === getPlayerId()) {
        removePlayerId();
    } else {
        setPlayerId(id);
    }
    return getPlayerId();
}

function showOnPlayerExists(id) {
    onIdChange((newId) => {
        document.getElementById(id).style.display = newId === null ? 'none' : '';
    });
}
