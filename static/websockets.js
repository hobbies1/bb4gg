export function setUpWebSockets (channel, handler) {
  var protocol = 'ws://';
  if (window.location.protocol === 'https:') {
    protocol = 'wss://';
  }
  var instanceName =
    document.querySelector('meta[name="instance-name"]').content;
  var url =
    protocol + window.location.host + '/' + instanceName + '/websockets/';
  var ws = new WebSocket(url);

  ws.onopen = function () {
    ws.send(channel);
  };

  ws.onmessage = handler;

  ws.onclose = function (event) {
    if(!event.wasClean) {
      document.getElementById('disconectMsg').style.display = '';
    }
  };
};
