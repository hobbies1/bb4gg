{-# OPTIONS_GHC -Wno-redundant-constraints #-}

module BB4GG.Web.Util.Splices ( Path
                              , assignmentSplices
                              , eitherSplice
                              , errorPageSplices
                              , digestiveSplices'
                              , gameInfoSplice
                              , gameInfoSplice'
                              , ifAttributeSplices
                              , ifSplices
                              , lockedTimeSplices
                              , maybeValueSplice
                              , normalGameSplices
                              , playerDataSplices
                              , rangeSplice
                              , runChildrenWith'
                              , showSplice
                              , stringSplice
                              ) where

import BB4GG.Core

import BB4GG.Types.Util (showPlayerStatus)
import BB4GG.Util (toRoundSeconds, sortMap)

import Control.Lens ((^.), (^?), view)
import Control.Monad (mfilter)
import Control.Monad.IO.Class (MonadIO)
import Data.Either.Combinators (maybeToLeft)
import Data.Function (on)
import Data.List (nub, sortOn)
import Data.Map.Syntax ((##))
import Data.Maybe (isJust)
import Data.String (fromString)
import Data.Text.Encoding (decodeUtf8)
import Data.Time (UTCTime)
import Data.Time.Clock.POSIX (utcTimeToPOSIXSeconds)
import Heist (Splices, getParamNode, localHS, AttrSplice)
import Heist.Interpreted ( Splice
                         , bindAttributeSplices
                         , bindSplices
                         , callTemplate
                         , callTemplateWithText
                         , mapSplices
                         , runChildren
                         , runChildrenWith
                         , runNodeList
                         , textSplice
                         )
import Heist.Splices (ifElseISplice)
import Text.Digestive (View, childErrors, errors)
import Text.Digestive.Heist (digestiveSplices)
import Text.XmlHtml (childNodes, getAttribute, tagName)

import qualified Data.ByteString.Char8 as BS
import qualified Data.Map as M
import qualified Data.Set as S
import qualified Data.Text as T


-- | For paths to URLs.
type Path = BS.ByteString

-- | Like 'textSplice', but uses a string instead.
stringSplice :: Monad m => String ->  Splice m
stringSplice = textSplice . fromString

-- | Passes the result of 'show' to 'stringSplice'.
showSplice :: (Monad m, Show a) => a ->  Splice m
showSplice = stringSplice . show

-- | The generated 'Splice' passes the given 'Text' to the template @error-span@ as @error-message@.
errorMessageSplice :: Monad m => T.Text -> Splice m
errorMessageSplice = callTemplateWithText "/error-span" . ("error-message" ##)

-- | 'Splices' for error messages.
-- The tag to use is <errors ref="someRef" />.
errorsSplices :: MonadFail m => View T.Text -> Splices (Splice m)
errorsSplices v =
  do "errors" ## errorsSpliceWith errors
     "child-errors" ## errorsSpliceWith childErrors
  where
    errorsSpliceWith extractor =
      do mRef <- getAttribute "ref" <$> getParamNode
         case mRef
           of Just ref ->
                case extractor ref v
                  of [] ->
                       return []
                     es ->
                       errorMessageSplice
                         . T.intercalate " "
                         $ map (<> ".") $ nub es
              Nothing ->
                fail "Encountered and error tag without a `ref` attribute."

-- | Combination of the standard digestive 'Splices' and the 'Splices' for errors.
digestiveSplices' ::
  (MonadFail m, MonadIO m) => View String -> Splices (Splice m)
digestiveSplices' v =
  let tView = T.pack <$> v in digestiveSplices tView <> errorsSplices tView

-- | 'Splices' for an error page.
errorPageSplices :: MonadFail m => String -> Maybe Path -> Splices (Splice m)
errorPageSplices err mRedirect =
  do ifSplices . M.singleton "redirect" $ isJust mRedirect
     foldMap (("redirect-location" ##) . textSplice . decodeUtf8) mRedirect
     "error-message" ## stringSplice err

-- | An @if@ tag implementation.
--
-- Every @if@ tag should contain a @var@ attribute, which is looked up in the 'Map' that is passed to this function, where missing values are assumed to be @False@.
-- The contents of the @if@ tag are then split at an (optional) @else@ tag and the first or second part is rendered depending on whether the boolean the @var@ attribute is referring to is @True@ or @False@.
--
-- If an @if@ tag does not contain a @var@ attribute, the associated 'Splice' @fail@s with an appropriate error message.
ifSplices :: MonadFail m => M.Map T.Text Bool -> Splices (Splice m)
ifSplices env =
  "if" ##
    do mVarName <- getAttribute "var" <$> getParamNode
       case mVarName
         of Nothing -> fail "Encountered an `if` tag without a `var` attribute."
            Just varName -> ifElseISplice $ Just True == M.lookup varName env

-- | 'Splices' containing all the information given (plus the prerendered player range).
--
-- Not all callers will need all the information included, but the overhead introduced by it should be negligible.
normalGameSplices :: Monad m => NormalGameID -> GameData -> Splices (Splice m)
normalGameSplices gID gd =
  do "game-id" ## showSplice $ gID
     "game-name" ## stringSplice $ gd ^. nameGD
     "game-comment" ##
       maybeValueSplice stringSplice . mfilter (not . null) . Just $
         gd ^. commentGD
     "game-bgg-id" ## maybeValueSplice showSplice $ gd ^. bggIDGD
     let minPlayers            = gd ^. minPlayersGD
         maxPlayers            = gd ^. maxPlayersGD
         recommendedMinPlayers = gd ^. recommendedMinPlayersGD
         recommendedMaxPlayers = gd ^. recommendedMaxPlayersGD
     "game-min" ## showSplice minPlayers
     "game-max" ## showSplice maxPlayers
     "game-player-range" ## (rangeSplice `on` show) minPlayers maxPlayers
     "game-recommended-min" ## showSplice recommendedMinPlayers
     "game-recommended-max" ## showSplice recommendedMaxPlayers
     "game-recommended-player-range" ##
       (rangeSplice `on` show) recommendedMinPlayers recommendedMaxPlayers
     "game-copies" ## showSplice $ gd ^. maxCopiesGD
     "game-availability" ## showSplice $ gd ^. isAvailableGD

-- | If parameters are equal, provides a single value, otherwise both.
rangeSplice :: Monad m => String -> String -> Splice m
rangeSplice from to =
  runChildrenWith . ("range" ##) $
    eitherSplice
      (\ single -> "single-value" ## stringSplice single)
      (\ (from', to') ->
         do "from-value" ## stringSplice from'
            "to-value" ## stringSplice to'
      )
      ( if from == to
          then Left from
          else Right (from, to)
      )

-- | Create a 'Splice' containing the name of the game and its player number tresholds.
--
-- The resulting 'Splice' calls the template @/instance/game-info/normal@ or @/instance/game-info/special@ according to the game type.
gameInfoSplice' :: Monad m => GameID -> GameData -> Splice m
gameInfoSplice' (SpecialGID NotPlayingSG) gd =
  callTemplateWithText "/instance/game-info/special"
    . ("game-name" ##)
    . fromString
    $ gd ^. nameGD
gameInfoSplice' (NormalGID gID) gd =
  callTemplate "/instance/game-info/normal" $ normalGameSplices gID gd

-- | Create a 'Splice' containing the name of the game and its player number tresholds.
gameInfoSplice :: Monad m => Games -> GameID -> Splice m
gameInfoSplice gs gID =
  case gs ^? gameDataG gID
    of Nothing -> errorMessageSplice "Error: game not found."
       Just gd -> gameInfoSplice' gID gd

-- | The `Splices` for accessing player data.
playerDataSplices :: Monad m => PlayerID -> PlayerData -> Splices (Splice m)
playerDataSplices pID pd =
  do "player-name" ## stringSplice $ pd ^. namePD
     "player-id" ## showSplice pID
     "player-status" ## stringSplice . showPlayerStatus $ pd ^. statusPD

-- | The `Splices` for accessing an assignment.
assignmentSplices
  :: Monad m => InstanceState -> Assignment -> Splices (Splice m)
assignmentSplices state assig =
  "assignment-games" ##
    mapSplices assignmentGameSplice
      . sortOn (\ (_, (gID, _)) -> state ^? gamesIS . gameDataG gID)
      $ M.toList assig
  where
    assignmentGameSplice (i, (gID, pIDs)) =
      runChildrenWith $
        do "game-info" ## gameInfoSplice (state ^. gamesIS) gID
           "game-playing-players" ##
             mapSplices id
               . sortMap (runChildrenWith . uncurry playerDataSplices)
               . M.map (view valueTS)
               . M.filterWithKey (\ pID _ -> S.member pID pIDs)
               $ state ^. playersIS
           "assignment-game-id" ## showSplice i

-- | 'Splices' that map @locked-time@ to number of seconds in the given `NominalDiffTime`.
-- Results in empty 'Splices' if the argument is @Nothing@.
lockedTimeSplices :: Monad m => Maybe UTCTime -> Splices (Splice m)
lockedTimeSplices =
  foldMap
    (("locked-time" ##) . showSplice . toRoundSeconds . utcTimeToPOSIXSeconds)

-- | A splice that returns its children up to an optional <else />-tag when the passed Maybe is Just.
-- In that case <value /> will be the result of the splice obtained by evaluating the function on the Just value.
-- When the passed Maybe is Nothing, returns everything after the <else />.
maybeValueSplice :: Monad m => (a -> Splice m) -> Maybe a -> Splice m
maybeValueSplice f =
  eitherSplice (("value" ##) . f) (const mempty) . maybeToLeft ()

-- | A splice that returns its children up to an <else />-tag only when passed 'Either' is 'Left' and everything afterwards when it is 'Right'.
-- In both cases the content is evaluated with the given 'Splices'.
eitherSplice
  :: Monad m => (a -> Splices (Splice m)) -> (b -> Splices (Splice m)) -> Either a b -> Splice m
eitherSplice f g e = getParamNode >>= (rewrite . childNodes)
  where
    rewrite nodes =
      case e
        of Left a -> runNodeListWith (f a) beforeElse
           Right b -> runNodeListWith (g b) $ drop 1 fromElse
      where
        (beforeElse, fromElse) = break (\n -> tagName n == Just "else") nodes
    runNodeListWith s nodes = localHS (bindSplices s) $ runNodeList nodes

-- An attribute splice called "if-then", that when given a value of the form "var=>attr", looks up var in the given 'Map' and sets the attribute attr if and only if the result is 'True'.
ifAttributeSplices :: Monad m => M.Map T.Text Bool -> Splices (AttrSplice m)
ifAttributeSplices vars =
  "if-then" ##
    \ val ->
      maybe (error "Erroneous if-then attribute encountered.") return $
        do let (var, attr') = T.breakOn thenSymbol val
           attr <- T.stripPrefix thenSymbol attr'
           bool <- vars M.!? var
           return [(attr, "") | bool]
  where
    thenSymbol = "=>"

-- | Like 'runChildrenWith', but also binds attribute splices.
runChildrenWith'
  :: Monad m => Splices (Splice m) -> Splices (AttrSplice m) -> Splice m
runChildrenWith' s as =
  localHS (bindSplices s . bindAttributeSplices as) runChildren
