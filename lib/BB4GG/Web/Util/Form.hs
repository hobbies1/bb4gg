{-# OPTIONS_GHC -Wno-redundant-constraints #-}

module BB4GG.Web.Util.Form ( conditionsWithCommas
                           , integralForm
                           , integralInRange
                           , isJustCondition
                           , isLegalName
                           , optionalIntegralForm
                           , optionalIntegralInRange
                           , orderCondition
                           , orderCondition'
                           , rangeCondition
                           ) where

import Control.Lens (Getter, (^.))
import Data.Char (isAlphaNum, isLatin1)
import Data.List (intercalate)
import Data.List.Extra (trim)
import Text.Digestive ( Form
                      , Formlet
                      , Result (..)
                      , conditions
                      , optionalStringRead
                      , resultMapError
                      , stringRead
                      , validate
                      , validateOptional
                      )

import qualified Data.Set as S


-- | Check whether a name is legal.
isLegalName ::
     Int                  -- ^ Maximal length allowed.
  -> S.Set Char           -- ^ Legal special characters.
  -> S.Set String         -- ^ Names that are already taken.
  -> String               -- ^ Name to be checked.
  -> Result String String -- ^ An 'Error' with a description if the name is not legal, a 'Success' with the trimmed version of the name otherwise.
isLegalName maxLength special takenNames name
  | name' == "" =
    Error "Name can not be empty"
  | length name' > maxLength =
    Error $ "Name can not be longer than " ++ show maxLength ++ " characters"
  | isIllegal name' =
    Error "Name contains illegal characters"
  | name' `S.member` takenNames =
    Error "Name is already taken"
  | otherwise =
    Success name'
  where
    name' = trim name
    isIllegal =
      not . all (\ c -> (isAlphaNum c && isLatin1 c) || c `S.member` special)

-- | Generates a form that tries to read an integral value and produces a descriptive error message referring to the given name in case of failure.
integralForm ::
  (Show a, Read a, Integral a, Monad m) => String -> Formlet String m a
integralForm name = stringRead (name ++ " is not an integer")

-- | Generates a form that tries to read an optional integral value and produces a descriptive error message referring to the given name  in case of failure.
optionalIntegralForm ::
    (Show a, Read a, Integral a, Monad m)
  => String
  -> Maybe a
  -> Form String m (Maybe a)
optionalIntegralForm name = optionalStringRead (name ++ " is not an integer")

-- | Generates a form for an integral value in a range.
integralInRange ::
     (Show a, Read a, Integral a, Ord a, Monad m)
  => String     -- ^ A name for the form field in question. (Used for error messages.)
  -> (a, a)     -- ^ The allowed range.
  -> Formlet String m a
integralInRange name range =
  validate (rangeCondition name range) . integralForm name

-- | Generates a form for an optional integral value in a range.
optionalIntegralInRange ::
     (Show a, Read a, Integral a, Ord a, Monad m)
  => String     -- ^ A name for the form field in question. (Used for error messages.)
  -> (a, a)     -- ^ The allowed range.
  -> Maybe a    -- ^ Possibly a default value.
  -> Form String m (Maybe a)
optionalIntegralInRange name range =
  validateOptional (rangeCondition name range) . optionalIntegralForm name

-- | Checks whether the given range contains the given element, creating a descriptive error message referring to the given name if it is not.
rangeCondition :: (Ord a, Show a) => String -> (a, a) -> a -> Result String a
rangeCondition name (l, u) x =
  if x >= l && x <= u
    then Success x
    else Error $ name ++ " is not in [" ++ show l ++ ", " ++ show u ++ "]"

-- | Checks whether a field of a value is smaller than another one.
orderCondition ::
     Ord a
  => String          -- ^ A name for the field that should be smaller.
  -> Getter s a      -- ^ 'Getter' for the field that should be smaller.
  -> String          -- ^ A name for the field that should be larger.
  -> Getter s a      -- ^ 'Getter' for the field that should be larger.
  -> s               -- ^ The value whose fields will be compared.
  -> Result String s -- ^ The value wrapped in a 'Success' if it passes the validation, a descriptive error message wrapped in an 'Error' otherwise.
orderCondition lowerName lowerGetter upperName upperGetter value =
  value
    <$ orderCondition'
         lowerName
         (value ^. lowerGetter)
         upperName
         (value ^. upperGetter)

-- | Checks whether a value is smaller than another one.
orderCondition' ::
     Ord a
  => String -- ^ A name for the value that should be smaller.
  -> a      -- ^ Value that should be smaller.
  -> String -- ^ A name for the value that should be larger.
  -> a      -- ^ Value that should be larger.
  -> Result String () -- ^ '()' in a 'Success' if it passes the validation, a descriptive error message wrapped in an 'Error' otherwise.
orderCondition' lowerName lowerVal upperName upperVal =
  if lowerVal <= upperVal
    then Success ()
    else Error $ lowerName ++ " has to be <= " ++ upperName

-- | Checks whether the given element is 'Just'.
isJustCondition :: String -> Maybe a -> Result String a
isJustCondition _ (Just a) = Success a
isJustCondition name Nothing = Error $ "No " ++ name ++ " given"

-- | Like 'conditions', but intercalates the resulting errors with @, @.
conditionsWithCommas :: [a -> Result String b] -> a -> Result String a
conditionsWithCommas l = resultMapError (intercalate ", ") . conditions l
