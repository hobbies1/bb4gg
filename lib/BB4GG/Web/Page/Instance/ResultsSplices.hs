module BB4GG.Web.Page.Instance.ResultsSplices ( renderResults
                                              , resultsSplices
                                              ) where

import BB4GG.Core

import BB4GG.Types.Util (readComputationStatus)
import BB4GG.Web.Util.Splices ( assignmentSplices
                              , ifSplices
                              , lockedTimeSplices
                              , playerDataSplices
                              , showSplice
                              , stringSplice
                              )

import Control.Lens ((^.), (^?), ix, to)
import Data.List (sortOn)
import Data.Map.Syntax ((##))
import Data.Maybe (isJust, mapMaybe)
import Data.Ord (Down (..))
import Data.Tuple (swap)
import Heist (Splices)
import Heist.Interpreted (Splice, mapSplices, runChildrenWith)
import Numeric.Extra (intToDouble)
import Text.Printf (printf)

import qualified Data.ByteString.Lazy.Char8 as BS
import qualified Data.Map as M
import qualified Data.Set as S


-- | 'Splices' for results (including possible errors).
resultsSplices ::
  (MonadBB4GGInstance' c m, MonadFail n) => m (Splices (Splice n))
resultsSplices =
  do state <- obtain
     case state ^. computationStatusIS
       of NotLockedCS (Left NoActivePlayersCE) ->
            return . ifSplices $ M.singleton "noActivePlayers" True
          NotLockedCS (Left (PlayersNotYetReadyCE ps)) ->
            return $ playersErrorSplices state "playersNotYetReady" ps
          NotLockedCS (Left (PlayersWithoutPreferenceCE ps)) ->
            return $ playersErrorSplices state "playersWithoutPreference" ps
          RecomputeCS ->
            return . ifSplices $ M.singleton "needToRecompute" True
          _ ->
            possibleAssignmentSplices
    where
      playersErrorSplices state name ps =
        do ifSplices . M.fromList $
             [(name, True), ("plural", S.size ps > 1)]
           "error-players" ##
             mapSplices (runChildrenWith . uncurry playerDataSplices)
               . sortOn swap
               . mapMaybe
                   (\ pID -> state ^? playersIS . ix pID . valueTS . to (pID, ))
               $ S.toList ps

-- | Assuming that there are no computation errors and that no recomputation is necessary, this produces 'Splices' for the remaining possibilities of failure or the possible assignments if there are some.
possibleAssignmentSplices ::
  (MonadBB4GGInstance' c m, MonadFail n) => m (Splices (Splice n))
possibleAssignmentSplices =
  do state <- obtain
     compStatus <- readComputationStatus $ state ^. computationStatusIS
     return $
       case compStatus ^? compCS
         of Nothing ->
              ifSplices $ M.singleton "notRunning" True
            Just comp ->
              let asss = comp ^. computedAssignmentsC
              in if M.null asss
                   then
                     ifSplices $ M.singleton "noPossibleAssignments" True
                   else
                     do let mUnlockTime = compStatus ^? unlockTimeCS
                            locked = isJust mUnlockTime
                            stillComputing = isJust $ comp ^. threadIdC
                        ifSplices . M.fromList $
                          [ ("assignmentsPossible", True)
                          , ("locked", locked)
                          , ("stillComputing", stillComputing)
                          ]
                        lockedTimeSplices mUnlockTime
                        "possible-assignments" ##
                            mapSplices (assignmentSplice state)
                              . sortOn (Down . snd . snd)
                              . M.assocs
                              $ asss
  where
    assignmentSplice state (i, (assignment, score)) =
      runChildrenWith $
        do "assignment-average-score" ## stringSplice . printf "%.2f" $
               intToDouble (score ^. sumAS)
             / intToDouble (length $ foldMap snd assignment)
           "assignment-minimum-score" ## showSplice $ score ^. minimumAS
           "assignment-id" ## showSplice $ i
           assignmentSplices state assignment

-- | Render the results (including possible errors).
renderResults :: MonadBB4GGInstance' c m => m BS.ByteString
renderResults =
  (maybe "Couldn't render results for websockets." fst <$>)
    . renderWithSplices "/instance/results/content"
    =<< resultsSplices
