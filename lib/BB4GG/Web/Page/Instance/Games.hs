{-# LANGUAGE QuasiQuotes #-}

module BB4GG.Web.Page.Instance.Games (modifyGamesPage) where

import BB4GG.Core

import BB4GG.Core.Computation.Handling (resetComputation)
import BB4GG.Util (batch, clamp, insertNew, sortMap)
import BB4GG.Web.Util.Form ( integralInRange
                           , isJustCondition
                           , isLegalName
                           , optionalIntegralInRange
                           , orderCondition'
                           )
import BB4GG.Web.Util.Splices ( digestiveSplices'
                              , errorPageSplices
                              , ifAttributeSplices
                              , ifSplices
                              , normalGameSplices
                              , runChildrenWith'
                              , showSplice
                              , stringSplice
                              )
import BB4GG.Web.Websockets.Broadcast (broadcastMain)

import Control.Applicative ((<|>))
import Control.Lens ((^.), at, modifying, over, set, use, view)
import Control.Monad (forM_, join, mfilter, void)
import Control.Arrow ((&&&))
import Data.Ix (inRange)
import Data.Functor ((<&>))
import Data.List (intercalate)
import Data.Map.Syntax ((##))
import Data.Maybe (fromMaybe, isJust)
import Data.Monoid (Any (..))
import Heist (Splices)
import Heist.Interpreted (Splice, mapSplices)
import Snap.Core (MonadSnap, getQueryParam, redirect)
import Text.Digestive ( Form
                      , Result (..)
                      , (.:)
                      , check
                      , resultMapError
                      , string
                      , validate
                      , validateM
                      )
import Text.Digestive.Snap (runForm)
import Text.RE.TDFA
import Text.Read (readMaybe)

import qualified Data.ByteString.Char8 as BS
import qualified Data.Map.Strict as M
import qualified Data.Set as S


-- | The page where you can modify the 'Games'.
modifyGamesPage :: MonadBB4GGInstance LockISt MonadSnap m => m ()
modifyGamesPage =
  join $ runUnlocked @ LockISt $
    do games <- use gamesIS
       midBS <- getQueryParam "id"
       let mgID  = (readMaybe . BS.unpack) =<< midBS
           mgd   = (`M.lookup` games) =<< mgID
       result <- runForm "modifyGames" $ modifyGamesForm mgd
       case (result, mgID, mgd)
         of ((_, Nothing), Just _, Nothing) ->
              return . respondWithSplices "/instance/error" $
                errorPageSplices "Game has been deleted" (Just ".")
            ((digestiveView, Nothing), _, _) ->
              return . respondWithSplices "/instance/modify-games" $
                do digestiveSplices' digestiveView
                   ifSplices . M.singleton "modify" $ isJust mgd
                   "max-game-name-length" ## showSplice maxGameNameLength
                   "max-game-comment-length" ## showSplice maxGameCommentLength
                   "max-users" ## showSplice userLimit
                   "selected-game-id" ## maybe (return []) showSplice mgID
                   "selected-game-name" ##
                     maybe
                       (return [])
                       (stringSplice . view nameGD . (games M.!))
                       mgID
                   gamesSplices games
            ((_, Just newGameData), _, _) ->
              do batch $
                   do modifying
                        gamesIS
                        (case mgd *> mgID
                           of Nothing  -> insertNew newGameData
                              Just gID -> M.insert gID newGameData
                        )
                      forM_ (mgd *> mgID) $
                        \ gID ->
                            modifying
                              ( playersIS
                              . traverse
                              . valueUnstampedTS
                              . prefPD
                              . at (NormalGID gID)
                              )
                              (>>= restrictPref
                                     (newGameData ^. minPlayersGD)
                                     (newGameData ^. maxPlayersGD)
                              )
                 resetComputation
                 return $
                   do broadcastMain
                      saveInstanceStateUnlocked
                      redirect "."
  where
    restrictNum mi ma = max mi . min ma
    restrictExtraPrefs mi ma =
      (Any . any (/= 0) . view extraScoresGP &&& id)
        . over extraScoresGP (M.filterWithKey $ \ k _ -> inRange (mi, ma) k)
    restrictPlayerNums mi ma gamePref =
      if not (inRange (mi, ma) (gamePref ^. minPlayersGP)) &&
         not (inRange (mi, ma) (gamePref ^. maxPlayersGP))
      then
        ( Any False
        , set scoreGP 0 . set minPlayersGP mi . set maxPlayersGP ma $ gamePref
        )
      else
        ( Any True
        , over minPlayersGP (restrictNum mi ma)
            . over maxPlayersGP (restrictNum mi ma)
            $ gamePref
        )
    restrictPref mi ma gamePref =
      case restrictExtraPrefs mi ma =<< restrictPlayerNums mi ma gamePref
        of (Any True, gp) -> Just gp
           (Any False, _) -> Nothing

-- | The form for creating or modifying a game.
-- If it is modifying a game, the function gets passed the current 'GameData' so the fields can be prefilled.
modifyGamesForm :: MonadBGG m => Maybe GameData -> Form String m GameData
modifyGamesForm mgd =
  validate val $
    do name <-
         "gameName" .:
           validate
             ( isLegalName
                 maxGameNameLength
                 (S.fromList " _+-=:;,.'?!€$%§&@#~*|/\\()[]{}")
                 S.empty
             )
             (string $ fmap (view nameGD) mgd)
       comment <-
         "gameComment" .:
           check "Can not be longer than 20 characters"
             ((<= maxGameCommentLength) . length)
             (string $ fmap (view commentGD) mgd)
       mBGGID <- "bggID" .:
         validateM valBGGID (string $ fmap show . view bggIDGD =<< mgd)
       mMinPlayers <- "min" .:
         optionalIntegralInRange "Min"
           (1, userLimit)
           (view minPlayersGD <$> mgd)
       mMaxPlayers <- "max" .:
         optionalIntegralInRange "Max"
           (1, userLimit)
           (view maxPlayersGD <$> mgd)
       recommendedMinPlayers <- "recommendedMin" .:
         optionalIntegralInRange
           "Recommended Min"
           (1, userLimit)
           (view recommendedMinPlayersGD <$> mgd)
       recommendedMaxPlayers <- "recommendedMax" .:
         optionalIntegralInRange
           "Recommended Max"
           (1, userLimit)
           (view recommendedMaxPlayersGD <$> mgd)
       maxCopies <- "maxCopies" .:
         integralInRange
           "Copies"
           (1, userLimit)
           (view maxCopiesGD <$> mgd <|> Just 1)
       pure 
         ( name
         , comment
         , mBGGID
         , mMinPlayers
         , mMaxPlayers
         , recommendedMinPlayers
         , recommendedMaxPlayers
         , maxCopies
         )
  where
    -- This makes the bggID also accept (parts of) the appropriate BGG URL.
    valBGGID "" = return $ Success Nothing
    valBGGID str =
      case readMaybe $ str ?=~/ [ed|^(.*/)?${id}([0-9]+)(/.*)?$///${id}|]
        of Just bggID ->
             retrieveBGGThing bggID <&> \case
               -- If an error occurs during fetching, we accept the result as to not block adding games when BGG is down for example.
               Left _ -> Success $ Just (bggID, Nothing)
               Right Nothing -> Error "BGG id does not exist"
               Right (Just bggThing) -> Success $ Just (bggID, Just bggThing)
           Nothing -> return $ Error "Could not read BGG id"
    val ( name
        , comment
        , mBGGID
        , mMinP
        , mMaxP
        , mRecomMinP
        , mRecomMaxP
        , maxCopies
        ) =
      do let mBggGame = snd =<< mBGGID
             mMinP' =
                   mMinP
               <|> fmap (clamp (1, userLimit))
                     (view minPlayersBGGT =<< mBggGame)
             mMaxP' =
                   mMaxP
               <|> fmap (clamp (1, userLimit))
                     (view maxPlayersBGGT =<< mBggGame)
         minP <- isJustCondition "minimum player number" mMinP'
         maxP <- isJustCondition "maximum player number" mMaxP'
         let recomMinP =
               fromMaybe minP $
                     mRecomMinP
                 <|> mfilter (inRange (1, userLimit))
                       (   S.lookupMin
                       .   view recommendedPlayersBGGT
                       =<< mBggGame
                       )
         let recomMaxP =
               fromMaybe maxP $
                     mRecomMaxP
                 <|> mfilter (inRange (1, userLimit))
                       (   S.lookupMax
                       .   view recommendedPlayersBGGT
                       =<< mBggGame
                       )
         void . resultMapError (intercalate ", ") . sequenceErr $
           [ orderCondition' "Min" minP "Max" maxP
           , orderCondition' "Min" minP "Recommended Min" recomMinP
           , orderCondition' "Recommended Max" recomMaxP "Max" maxP
           , orderCondition'
               "Recommended Min"
               recomMinP
               "Recommended Max"
               recomMaxP
           ]
         return $ GameData
           { _nameGD = name
           , _commentGD = comment
           , _bggIDGD = fmap fst mBGGID
           , _minPlayersGD = minP
           , _maxPlayersGD = maxP
           , _recommendedMinPlayersGD = recomMinP
           , _recommendedMaxPlayersGD = recomMaxP
           , _maxCopiesGD = maxCopies
           , _isAvailableGD = maybe True (view isAvailableGD) mgd
           }
    sequenceErr [] = Success []
    sequenceErr ((Error e) : l) =
      case sequenceErr l
        of Error es -> Error $ e : es
           _ -> Error [e]
    sequenceErr ((Success s) : l) = (s :) <$> sequenceErr l

-- | 'Splices' for displaying games.
gamesSplices :: Monad m => Games -> Splices (Splice m)
gamesSplices =
  ("games" ##)
    . mapSplices id
    . sortMap
        (\ (gID, game) ->
           runChildrenWith'
             (normalGameSplices gID game)
             (ifAvailableSplices game)
        )
  where
    ifAvailableSplices game =
      ifAttributeSplices $ M.fromList [("available", game ^. isAvailableGD)]
