module BB4GG.Web.Page.Instance.Results (resultsPage) where

import BB4GG.Core

import BB4GG.Core.Computation.Handling (startComputation)
import BB4GG.Web.Page.Instance.ResultsSplices (resultsSplices)

import Control.Lens (use)
import Snap.Core (MonadSnap)


-- | The page where possible assignments are displayed.
resultsPage :: MonadBB4GGInstance LockISt MonadSnap m => m ()
resultsPage =
  do runUnlocked @ LockISt $
       do computationStatus <- use computationStatusIS
          case computationStatus
            of RecomputeCS -> startComputation
               _           -> return ()
     respondWithSplices "/instance/results/page" =<< resultsSplices
