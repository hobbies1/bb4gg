module BB4GG.Web.Page.Instance.Main ( instanceMainPage
                                    , renderMainContent
                                    ) where

import BB4GG.Core

import BB4GG.Types.Util ( assignedPlayers
                        , displayPlayerStatus
                        , gamePreferenceScores
                        , preferringPlayers
                        , readMaybePlayerStatus
                        , requiresReady
                        , showPlayerStatus
                        , waitingPlayers
                        )
import BB4GG.Util (sortMap)
import BB4GG.Web.Util.Splices ( assignmentSplices
                              , ifSplices
                              , normalGameSplices
                              , playerDataSplices
                              , showSplice
                              , stringSplice
                              )

import Control.Lens ((^.), ix, maximumOf, to, view)
import Data.List (sortOn)
import Data.Map.Syntax ((##))
import Data.Maybe (fromMaybe, isJust, mapMaybe)
import Heist (Splices, getParamNode)
import Heist.Interpreted ( Splice
                         , callTemplate
                         , mapSplices
                         , runChildrenWith
                         , textSplice
                         )
import Snap.Core (MonadSnap)
import Text.XmlHtml (getAttribute)

import qualified Data.ByteString.Lazy as BS
import qualified Data.Map.Strict as M
import qualified Data.Ord as Ord
import qualified Data.Text as T


-- | The main page of the instance displaying the current assignment and the current preferences.
instanceMainPage :: MonadBB4GGInstance' MonadSnap m => m ()
instanceMainPage =
  respondWithSplices "/instance/main/page"
    . instanceMainPageSplices
    =<< obtain

-- | Generates the 'Splices' that are used to render the instance main page.
instanceMainPageSplices :: MonadFail m => InstanceState -> Splices (Splice m)
instanceMainPageSplices state =
  do ifSplices . M.fromList $
       [ ("requireReady", requiresReady state)
       , ("gamesInProgress", gamesInProgress)
       ]
     assignmentSplices state $ state ^. currentAssigIS . valueTS
     playersLinesSplices state
     preferencesTableSplices state
  where
    gamesInProgress = not . M.null $ state ^. currentAssigIS . valueTS

-- | 'Splices' for rendering names of the players in groups according to their statuses.
playersLinesSplices :: MonadFail m => InstanceState -> Splices (Splice m)
playersLinesSplices state =
  "players-line" ##
    do mStatusText <- getAttribute "status" <$> getParamNode
       case mStatusText
         of Nothing ->
              fail
                "Encountered a `players-line` tag without a `status` attribute."
            Just statusText ->
              if statusText == "Playing"
                then
                  lineSplice Nothing
                else
                  case readMaybePlayerStatus . T.unpack $ statusText
                    of Nothing -> fail "Can't parse player status."
                       Just ps' -> lineSplice $ Just ps'
  where
    lineSplice mStatus =
      callTemplate "/instance/main/players-line" $
        do "players-line-title" ##
             textSplice $ maybe "Playing" displayPlayerStatus mStatus
           "players" ##
             mapSplices id
               . sortMap (playerSpliceWith $ isJust mStatus)
               . M.filterWithKey (predicateFor mStatus)
               . M.map (view valueTS)
               $ state ^. playersIS
    playerSpliceWith renderArrows (pID, pd) =
      runChildrenWith $
        do playerDataSplices pID pd
           "player-arrows" ##
             if renderArrows
               then arrowsSplice $ pd ^. statusPD
               else return []
    arrowsSplice ReadyPS =
      arrowSplice "double-down" InactivePS
    arrowsSplice WantsToPlayPS =
      mapSplices (uncurry arrowSplice) $
        [("up", ReadyPS) | requiresReady state] ++ [("down", InactivePS)]
    arrowsSplice InactivePS =
      arrowSplice "up" WantsToPlayPS
    arrowSplice direction targetStatus =
      runChildrenWith $
        do "arrow-target-status" ## stringSplice $ showPlayerStatus targetStatus
           "arrow-direction" ## textSplice direction
    predicateFor Nothing pID _ =
      M.member pID $ assignedPlayers state
    predicateFor (Just status) pID pd =
      pd ^. statusPD == status && (not . M.member pID $ assignedPlayers state)

-- | 'Splices' for rendering a summary of players' preferences if there are "waiting" players.
preferencesTableSplices :: Monad m => InstanceState -> Splices (Splice m)
preferencesTableSplices state =
  "preferences" ##
    if M.null $ waitingPlayers state
      then
        return []
      else
        runChildrenWith $
          "game-row" ##  mapSplices gamePrefSplice preferredGames
  where
    gamePrefSplice (ngID, gd, players, scoreSum, averageScore) =
      runChildrenWith $
        do normalGameSplices ngID gd
           "score-sum" ## showSplice scoreSum
           "average-score" ## showSplice averageScore
           "preferring-players" ##
             mapSplices id . sortMap playerSplice $ players
    playerSplice (pID, pd) = runChildrenWith $ playerDataSplices pID pd
    preferredGames =
      sortOn popularity . mapMaybe processGame . M.toList $ state ^. gamesIS
    popularity (_, gd, players, scoreSum, avgScore) =
      Ord.Down (gd ^. isAvailableGD, scoreSum, avgScore, length players, gd)
    processGame (ngID, gd) =
      if M.null players
        then Nothing
        else Just (ngID, gd, players, scoreSum, averageScore)
      where
        players =
          M.map (view valueTS) $ preferringPlayers (waitingPlayers state) gID
        gID = NormalGID ngID
        scoreSum = sum $ maxScore <$> players
        maxScore =
          fromMaybe 0
            . maximumOf (prefPD . ix gID . to gamePreferenceScores . traverse)
        averageScore = scoreSum `div` max 1 (length players)

-- | Render the content of the instance main page.
renderMainContent :: MonadBB4GGInstance' c m => m BS.ByteString
renderMainContent =
  (maybe "Couldn't render content for websockets." fst <$>)
    . renderWithSplices "/instance/main/content"
    . instanceMainPageSplices
    =<< obtain
