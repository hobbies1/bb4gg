module BB4GG.Web.Page.Instance.Player (modifyPlayerPage) where

import BB4GG.Core

import BB4GG.Core.Computation.Handling (resetComputation)
import BB4GG.Types.Util ( displayPlayerStatus
                        , hasActivePreference
                        , isInGame
                        , isPlayerInactive
                        , requiresReady
                        , showPlayerStatus
                        )
import BB4GG.Util (insertNew)
import BB4GG.Web.Util.Form ( conditionsWithCommas
                           , isLegalName
                           , optionalIntegralInRange
                           , orderCondition
                           , rangeCondition
                           )
import BB4GG.Web.Util.Splices ( digestiveSplices'
                              , errorPageSplices
                              , ifAttributeSplices
                              , ifSplices
                              , lockedTimeSplices
                              , normalGameSplices
                              , showSplice
                              , stringSplice
                              )
import BB4GG.Web.Websockets.Broadcast (broadcastMain)

import Control.Applicative ((<|>))
import Control.Arrow ((&&&))
import Control.Lens ((^.), (^?), assign, ix, modifying, orOf, view)
import Control.Monad (join, mfilter, void, when)
import Control.Monad.IO.Class (MonadIO)
import Data.Aeson.Text (encodeToLazyText)
import Data.List (intercalate, nub, sort, sortOn)
import Data.List.Extra (trim)
import Data.Map.Syntax ((##))
import Data.Maybe (fromJust, fromMaybe, isJust, isNothing)
import Data.String (IsString, fromString)
import Data.Time (UTCTime, getCurrentTime)
import Heist (Splices)
import Heist.Interpreted (Splice, mapSplices, runChildrenWith, textSplice)
import Snap.Core (MonadSnap, getQueryParam, redirect)
import Text.Digestive ( Form
                      , Result (..)
                      , View
                      , bool
                      , check
                      , choiceWith
                      , fieldInputText
                      , string
                      , validate
                      , validateOptional
                      , (.:)
                      )
import Text.Digestive.Snap (runForm)
import Text.Parsec (ParseError, eof, many1, optionMaybe, parse, sepBy, sepBy1)
import Text.Parsec.Char (char, digit, space)
import Text.Read (readMaybe)

import qualified Data.ByteString.Char8 as BS
import qualified Data.Foldable as F
import qualified Data.Map.Strict as M
import qualified Data.Set as S
import qualified Data.Text as T
import qualified Data.Text.Lazy as TL
import Control.Monad.Random.Lazy (liftIO)


-- | The page where you can create or modify a player.
modifyPlayerPage :: MonadBB4GGInstance LockISt MonadSnap m => m ()
modifyPlayerPage =
  join $ runUnlocked @ LockISt $
    do state <- obtain
       midBS <- getQueryParam "id"
       let mpID    = (readMaybe . BS.unpack) =<< midBS
           mpd     = (`M.lookup` players) =<< mpID
           players = state ^. playersIS
       result <-
         runForm "modifyPlayerForm" $
           modifyPlayerForm
             (view valueTS <$> mpd)
             ( isLegalName
                 maxPlayerNameLength
                 (S.fromList " -_'")
                 ( S.fromList
                 . M.elems
                 . fmap (view $ valueTS . namePD)
                 . maybe players (`M.delete` players)
                 $ mpID
                 )
             )
             (state ^. gamesIS)
             (requiresReady state)
       case (result, mpID, mpd)
         of ((_, Nothing), Just _, Nothing) ->
              return . respondWithSplices "/instance/error" $
                errorPageSplices "Player has been deleted" (Just "../..")
            ((digestiveView, Nothing), _, _) ->
              return $ respondWithSplices' "/instance/modify-player"
                ( modifyPlayerSplices
                    (state ^? computationStatusIS . unlockTimeCS)
                    mpID
                    mpd
                    state
                    digestiveView
                )
                (ifAttributeSplices $ M.fromList [("new", isNothing mpID)])
            ((_ , Just newPlayerData), _, _) ->
              do now <- liftIO getCurrentTime
                 case view valueTS <$> mpd of
                   Nothing ->
                     modifying playersIS (insertNew $ stamp now newPlayerData)
                       >> resetComputation
                   Just playerData ->
                     do modifying
                          playersIS
                          (M.insert (fromJust mpID) $ stamp now newPlayerData)
                        if ( playerData ^. statusPD == InactivePS
                           && newPlayerData ^. statusPD /= InactivePS
                           ) ||
                             (  playerData ^. statusPD /= InactivePS
                             && newPlayerData ^. statusPD == InactivePS
                             )
                          then
                            resetComputation
                          else
                            when (playerData /= newPlayerData) $
                              case state ^. computationStatusIS
                                of NotLockedCS _ ->
                                     resetComputation
                                   UnlockAtCS lockedUntil mVar ->
                                     assign
                                       computationStatusIS
                                       (RecomputeAtCS lockedUntil mVar)
                                   _ ->
                                     return ()
                 return $
                   do broadcastMain
                      saveInstanceStateUnlocked
                      redirect "../.."

-- | The form for creating or modifying a player.
modifyPlayerForm ::
  MonadIO m
  => Maybe PlayerData                 -- ^ For prefilling the fields when modifying a 'Player'.
  -> (String -> Result String String) -- ^ The function passed to 'validate' to determine if the entered name is legal.
  -> Games                            -- ^ The list of all games.
  -> Bool                             -- ^ Whether to show the 'Ready' option.
  -> Form String m PlayerData
modifyPlayerForm mPD nameValidation gs showReady =
  check "You must select at least one available game"
    (\ pd -> hasActivePreference gs (pd ^. prefPD) || isPlayerInactive pd)
    ( do name <- "name" .:
           validate nameValidation (string $ fmap (view namePD) mPD)
         submitReady <- "submitReady" .: fmap (/= "") (string Nothing)
         status <- "status" .:
           choiceWith
             ( map
                 ( (T.pack . showPlayerStatus . fst &&& id)
                 . (id &&& displayPlayerStatus)
                 )
                 ([ReadyPS | showReady] ++ [WantsToPlayPS, InactivePS])
             )
           (view statusPD <$> mPD <|> Just WantsToPlayPS)
         preferences <-
             fmap (foldr M.union M.empty)
           . sequenceA
           . ( notPlayingPreferenceForm
                 (mPD ^? traverse . prefPD . ix (SpecialGID NotPlayingSG))
             :)
           . M.elems
           . M.mapWithKey (preferenceForm $ fmap (view prefPD) mPD)
           $ gs
         return $
           PlayerData
             { _namePD   = name
             , _statusPD = if submitReady then ReadyPS else status
             , _prefPD   = preferences
             }
     )

-- | The form for entering the players 'Preference' for not playing.
-- If a player is being modified, the function gets passed the current 'Preference' so the fields can be prefilled.
notPlayingPreferenceForm ::
  Monad m
  => Maybe GamePreference
  -> Form String m Preference
notPlayingPreferenceForm mgp =
  fmap (M.singleton $ SpecialGID NotPlayingSG)
    . (labelOf (SpecialGID NotPlayingSG) .:)
    $ GamePreference
        <$> "pref" .:
              ( fmap (fromMaybe $ fst prefRange)
                  . optionalIntegralInRange "Preference" prefRange
                  . fmap (view scoreGP)
              ) mgp
        <*> pure (gd ^. minPlayersGD)
        <*> pure (gd ^. maxPlayersGD)
        <*> pure M.empty
        <*> pure True
  where
    gd = specialGameData NotPlayingSG
    prefRange = specialGamePreferenceRange NotPlayingSG

-- | The form for entering the players 'Preference' for a single game.
-- If a player is being modified, the function gets passed the current 'Preference' so the fields can be prefilled.
preferenceForm ::
  Monad m
  => Maybe Preference
  -> NormalGameID
  -> GameData
  -> Form String m Preference
preferenceForm pref ngID gd =
  fmap (M.fromList . F.toList . fmap (NormalGID ngID,))
    . (labelOf (NormalGID ngID) .:)
    . validateOptional (orderCondition "Min" minPlayersGP "Max" maxPlayersGP)
    . validate
        ( \ gp ->
            if gp ^. scoreGP == 0 && all (== 0) (gp ^. extraScoresGP)
              then
                if gp ^. isActiveGP
                  then Error "Game is active but associated preferences are 0"
                  else Success Nothing
              else
                Success $ Just gp
        )
    $ GamePreference
        <$> "pref" .:
              ( fmap (fromMaybe 0)
                  . optionalIntegralInRange "Preference" (0, maxPreference)
                  . mfilter (> 0)
                  . fmap (view scoreGP)
              ) mgp
        <*> "minP" .:
              ( fmap (fromMaybe $ gd ^. recommendedMinPlayersGD)
                  . optionalIntegralInRange
                      "Min"
                      (gd ^. minPlayersGD, gd ^. maxPlayersGD)
                  . fmap (view minPlayersGP)
              ) mgp
        <*> "maxP" .:
              ( fmap (fromMaybe $ gd ^. recommendedMaxPlayersGD)
                  . optionalIntegralInRange
                      "Max"
                      (gd ^. minPlayersGD, gd ^. maxPlayersGD)
                  . fmap (view maxPlayersGP)
              ) mgp
        <*> "extraPrefs" .: validate (extraPrefsValidator gd)
              (string $ fmap (unparseExtraPrefs . view extraScoresGP) mgp)
        <*> "active" .: bool (fmap (view isActiveGP) mgp)
  where mgp = M.lookup (NormalGID ngID) =<< pref

-- | Validates that a provided string is in the extra preferences format (for a normal game).
-- Returns the described preference map if successfull, an error message otherwise.
extraPrefsValidator :: GameData -> String -> Result String (M.Map Int Score)
extraPrefsValidator gd str =
  case parseExtraPrefs str
    of Left parseError ->
         Error $ "Can't parse extra preferences: " ++ show parseError
       Right extraPrefs ->
         conditionsWithCommas
           ( (map keyCondition . M.keys $ extraPrefs)
               ++ (map elemCondition . nub . M.elems $ extraPrefs)
           )
           extraPrefs
         where
           keyCondition num _ =
             rangeCondition
               ("Player number " ++ show num)
               (gd ^. minPlayersGD, gd ^. maxPlayersGD)
               num
           elemCondition pref _ =
             rangeCondition ("Preference " ++ show pref) (0, maxPreference) pref

-- | Generates the 'Splices' required to render the page for adding or modifying a player.
modifyPlayerSplices ::
     (MonadFail m, MonadIO m)
  => Maybe UTCTime -- ^ Whether / until when the results are locked.
  -> Maybe PlayerID
  -> Maybe (Timestamped PlayerData)
  -> InstanceState
  -> View String
  -> Splices (Splice m)
modifyPlayerSplices mUnlockTime mPID mPDTS state digestiveView =
  do "max-player-name-length" ## showSplice maxPlayerNameLength
     ifSplices . M.fromList $
       [ ("modify", isJust mPID)
       , ("requireReady", requiresReady state)
       , ("inGame", maybe False (isInGame state) mPID)
       , ("locked", isJust mPID && isJust mUnlockTime)
       , ("noExtraPrefs", not . any hasExtraPrefs $ M.keys games)
       ]
     digestiveSplices' digestiveView
     "init-is-preferred" ## initIsPreferredSplice
     "player-id" ## maybe (return []) showSplice mPID
     "player-name" ## maybe (return []) (stringSplice . view namePD) mPD
     lockedTimeSplices mUnlockTime
     let (npMin, npMax) = specialGamePreferenceRange NotPlayingSG
     "not-playing-min-preference" ## showSplice npMin
     "not-playing-max-preference" ## showSplice npMax
     "max-preference" ## showSplice maxPreference
     "normal-games" ##
       mapSplices normalGameSplice
         . sortOn ((not . view isAvailableGD . snd) &&& snd)
         $ M.toList games
  where
    games = state ^. gamesIS
    mPD = view valueTS <$> mPDTS
    -- It is necessary to check this via the digestiveView since, when the last submission produced an error, the things that will be pre-entered can be found there
    fieldNonEmpty field ngID =
      not
        . null
        . trim
        . T.unpack
        . fieldInputText (labelOf (NormalGID ngID) <> "." <> field)
        $ digestiveView
    hasExtraPrefs = fieldNonEmpty "extraPrefs"
    hasPref = fieldNonEmpty "pref"
    initIsPreferredSplice =
      textSplice
        . TL.toStrict
        . encodeToLazyText
        . M.mapWithKey (\ ngID _ -> hasExtraPrefs ngID || hasPref ngID)
        $ games
    normalGameSplice (gID, gd) =
      runChildrenWith $
        do normalGameSplices gID gd
           "game-activity" ##
             showSplice $
               orOf (traverse . prefPD . ix (NormalGID gID) . isActiveGP) mPD

-- | Create a label from a game ID.
labelOf :: IsString a => GameID -> a
labelOf (NormalGID ngID) = fromString $ "game-" ++ show ngID
labelOf (SpecialGID sg) = fromString $ "game-" ++ show sg

-- | Parses a string of extra preferences, returning the result or a 'ParseError'.
-- The string needs to contain space separated blocks of the form "range,range,...,range:score" where range is either a (natural) number or "number-number".
-- Example: "1-2,5:3 3,7-8,10-11:0 4:1".
parseExtraPrefs :: String -> Either ParseError (M.Map Int Score)
parseExtraPrefs = parse extraPrefs ""
  where
    extraPrefs =
      M.fromList . concat <$> sepBy singleExtraPref (many1 space) <* eof
    singleExtraPref =
      do playerNumbers <- rangeList
         void $ char ':'
         pref <- natNum
         return $ map (,pref) playerNumbers
    rangeList =
      concat <$> sepBy1 range (char ',')
    range =
      do a <- natNum
         mb <- optionMaybe (char '-' *> natNum)
         return [a..fromMaybe a mb]
    natNum =
      read <$> many1 digit

-- | Returns the internal representation of the extra preferences to the string format in which it needs to be entered.
-- Note that
-- @ parseExtraPrefs . unparseExtraPrefs == id @
-- but
-- @ unparseExtraPrefs . parseExtraPrefs != id @
unparseExtraPrefs :: M.Map Int Score -> String
unparseExtraPrefs extraPrefs =
  unwords $
    M.foldrWithKey
      (\ score rangesStr -> ((rangesStr ++ ":" ++ show score) :))
      []
      numbersStringByScore
  where
    numbersStringByScore =
      fmap
        (intercalate "," . map rangeToString . takeRanges . sort)
        numbersByScore
    rangeToString (a, b)
      | a == b    = show a
      | otherwise = show a ++ "-" ++ show b
    takeRanges [] = []
    takeRanges (x : xs) =
      case takeRanges xs
        of ((a, b) : rs) | a == x+1 -> (x, b) : rs
           rs                       -> (x, x) : rs
    numbersByScore =
      M.foldrWithKey
        (\ num score -> M.insertWith (++) score [num])
        M.empty
        extraPrefs
