module BB4GG.Web.Page.Main (mainPage) where

import BB4GG.Core

import BB4GG.Web.Util.Form (isLegalName)
import BB4GG.Web.Util.Splices (digestiveSplices', showSplice)
import BB4GG.Util.Monad.Obtain (obtaining)

import Control.Monad (filterM)
import Control.Monad.Extra (unlessM)
import Data.Map.Syntax ((##))
import Data.String (fromString)
import Heist (Splices)
import Heist.Interpreted (Splice, mapSplices, runChildrenWithText)
import Snap.Core (MonadSnap, redirect)
import Text.Digestive (Form, Result, string, validate, (.:))
import Text.Digestive.Snap (runForm)

import qualified Data.Set as S


-- | The main page displaying the current assignment and the current preferences.
mainPage :: MonadBB4GG MonadSnap m => m ()
mainPage =
  runUnlocked' $
    do instanceNames <- getInstanceNames
       result <-
         runForm "newInstanceForm"
           . newInstanceForm
           -- It is important that an underscore is not allowed here, so that instnace names do not conflict with internal URLs.
           . isLegalName maxInstanceNameLength (S.fromList "-")
           $ instanceNames
       case result
         of (view, Nothing) ->
              do listedInstancesSplices <-
                   instancesSplices <$>
                     filterM
                       ( fmap (Just True ==)
                       . runInstance (obtaining $ configurationIS . listedIC)
                       )
                       (S.toAscList instanceNames)
                 respondWithSplices "/main" $
                   do "max-instance-name-length" ##
                        showSplice maxInstanceNameLength
                      listedInstancesSplices
                      digestiveSplices' view
            (_, Just name) ->
              do unlessM (newInstance name) $
                   error "Instance with given name already exists"
                 redirect . fromString $ "/" ++ name ++ "/configure/"

-- | The form for creating an instance.
newInstanceForm ::
  Monad m
  => (String -> Result String String) -- ^ The function passed to 'validate' to determine if the entered name is legal.
  -> Form String m String
newInstanceForm nameValidation =
  "name" .: validate nameValidation (string Nothing)

-- | 'Splices' for displaying instance names.
instancesSplices :: Monad m => [String] -> Splices (Splice m)
instancesSplices =
  ("instances" ##)
    . mapSplices (\ s -> runChildrenWithText ("name" ## fromString s))
