module BB4GG.Web.Websockets.Broadcast
  ( broadcastAssignmentChosen
  , broadcastMain
  , broadcastResults
  ) where

import BB4GG.Core

import BB4GG.Util.Monad.Obtain (obtaining)
import BB4GG.Web.Page.Instance.Main (renderMainContent)
import BB4GG.Web.Page.Instance.ResultsSplices (renderResults)
import BB4GG.Types.Util (readComputationStatus) 

import Control.Lens (view)
import Data.Aeson (encode)


-- | Broadcasts the results via websockets.
broadcastResults :: MonadBB4GGInstance LockIBr c m => m ()
broadcastResults =
  runUnlocked @ LockIBr $
    do broadcastWS . ResultsHtmlMTRC =<< renderResults
       broadcastWS
         . ResultsJSONMTRJC
         . encode
         . fmap (view computedAssignmentsC)
         =<< readComputationStatus
         =<< obtaining computationStatusIS

-- | Broadcasts via websockets that a result has been chosen.
broadcastAssignmentChosen :: MonadBB4GGInstance LockIBr c m => m ()
broadcastAssignmentChosen =
  runUnlocked @ LockIBr $
    do broadcastWS AssignmentChosenMTRC
       broadcastWS AssignmentChosenMTRJC

-- | Broadcasts the instance main page content via websockets.
broadcastMain :: MonadBB4GGInstance LockIBr c m => m ()
broadcastMain =
  runUnlocked @ LockIBr $
    do broadcastWS . MainHtmlMTMC =<< renderMainContent
       broadcastWS MainUpdatePingMUPC
