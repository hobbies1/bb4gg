{-# OPTIONS_GHC -Wno-unused-top-binds #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE StandaloneKindSignatures #-}
{-# LANGUAGE TemplateHaskell #-}

-- | This module describes a websockets server which provides multiple channels.
-- A client can connect to one of the channels and subsequently obtain all broadcasts on that channel.
module BB4GG.Web.Websockets.Server
  ( Channel (..)
  , HasProtocol (toMessageData)
  , MessageType (..)
  , WSServer (..)
  , broadcastWSS
  , hasClientsWSS
  , serverAppWSS
  , initWebsocketsServer
  ) where

import Control.Arrow ((&&&))
import Control.Lens ( Traversal'
                    , foldOf
                    , ix
                    , makeLenses
                    , notNullOf
                    , over
                    , preview
                    , set)
import Control.Monad (forM_, liftM2)
import Control.Monad.Extra (whenJustM)
import Control.Concurrent.MVar (MVar, modifyMVar_, newMVar, readMVar)
import Control.Exception (finally)
import Data.Ix (Ix)
import Data.List.Extra (enumerate)
import Data.Singletons (Sing, SingI, sing, fromSing)
import Data.Singletons.TH (genSingletons)
import Data.Time.Clock (UTCTime, getCurrentTime)
import System.Random (randomIO)
import Text.Read (readMaybe)

import qualified Data.ByteString.Lazy as BS
import qualified Data.Array as A
import qualified Data.Map.Strict as M
import qualified Data.Text.Lazy as T
import qualified Network.WebSockets as WS


-- | An enum type describing the possible websocket channels.
data Channel =
    MainChannel -- ^ The channel for the instance main page.
  | MainUpdatePingChannel -- ^ The channel for state updates in JSON.
  | ResultsChannel -- ^ The channel for the results page.
  | ResultsJSONChannel -- ^ The channel for result updates in JSON.
  deriving (Bounded, Enum, Eq, Ix, Ord, Read, Show)

-- | Generates various things needed for the 'reflectChannel' implementation.
genSingletons [''Channel]

-- | Demotes a member of 'Channel' from the type level to the value level.
reflectChannel :: forall (c :: Channel) . SingI c => Channel
reflectChannel = fromSing (sing :: Sing c)

-- | Describes the messages that can be sent via a channel.
-- A 'SingI' instance was generated automatically above for all types of kind 'Channel'.
class SingI c => HasProtocol (c :: Channel) where
  data MessageType c
  toMessageData :: MessageType c -> BS.ByteString

instance HasProtocol 'ResultsChannel where
  data MessageType 'ResultsChannel =
    ResultsHtmlMTRC BS.ByteString | AssignmentChosenMTRC
  toMessageData (ResultsHtmlMTRC bs) = bs
  toMessageData AssignmentChosenMTRC = "ASSIGNMENT CHOSEN"

instance HasProtocol 'ResultsJSONChannel where
  data MessageType 'ResultsJSONChannel =
    ResultsJSONMTRJC BS.ByteString | AssignmentChosenMTRJC
  toMessageData (ResultsJSONMTRJC bs) = bs
  toMessageData AssignmentChosenMTRJC = "ASSIGNMENT CHOSEN"

instance HasProtocol 'MainChannel where
  data MessageType 'MainChannel = MainHtmlMTMC BS.ByteString
  toMessageData (MainHtmlMTMC bs) = bs

instance HasProtocol 'MainUpdatePingChannel where
  data MessageType 'MainUpdatePingChannel = MainUpdatePingMUPC
  toMessageData MainUpdatePingMUPC = "UPDATE"

-- | We use the time of connection and a random salt (generated when connecting) to identify clients.
type ClientID = (UTCTime, Int)
-- | The only information for each client we need is the connection itself.
type Client = WS.Connection

-- | The state of a channel.
data ChannelState = ChannelState
  { _clientsCS :: M.Map ClientID Client -- ^ The clients subscribed to this channel.
  , _lastMessageCS :: Maybe BS.ByteString -- ^ The most recently broadcast message if there already was one.
  }

makeLenses ''ChannelState

-- Initializes a 'ChannelState'.
initialChannelState :: ChannelState
initialChannelState =
  ChannelState
    { _clientsCS = M.empty
    , _lastMessageCS = Nothing
    }

-- The state of the whole server.
newtype State = State
  { _channelsS :: A.Array Channel ChannelState -- ^ The states of all channels.
  }

makeLenses ''State

-- | A lens for accessing a specific channel.
channelS :: Channel -> Traversal' State ChannelState
channelS channel = channelsS . ix channel

-- | Creates an array initialized from all inhabitants of i.
arrayFromFinite :: (Bounded i, Enum i, Ix i) => (i -> e) -> A.Array i e
arrayFromFinite f = A.array (minBound, maxBound) $ map (id &&& f) enumerate

-- | Initializes a 'State'.
initialState :: State
initialState =
  State
    { _channelsS = arrayFromFinite $ const initialChannelState
    }

-- | The information necessary to interact with the server.
data WSServer = WSServer
  { _serverAppWSS  :: WS.ServerApp -- ^ Handles the connections.
  , _broadcastWSS  :: forall c . HasProtocol c => MessageType c -> IO () -- ^ Broadcasts a message to the channel 'c'.
  , _hasClientsWSS :: Channel -> IO Bool -- ^ Whether there are any clients registered to the specified channel.
  }

makeLenses ''WSServer

-- | Creates a websocket server.
-- Returns the server, an action that broadcasts, and an action that returns how many clients are subscribed to a specified channel.
initWebsocketsServer :: IO WSServer
initWebsocketsServer =
  do stateMVar <- newMVar initialState
     return $
       WSServer
         { _serverAppWSS = handlePending stateMVar
         , _broadcastWSS = broadcast stateMVar
         , _hasClientsWSS = hasClients stateMVar
         }

-- | The server.
-- Registers clients.
handlePending :: MVar State -> WS.PendingConnection -> IO ()
handlePending stateMVar pending =
  do conn <- WS.acceptRequest pending
     WS.withPingThread conn 30 (return ()) $
       do firstMsg <- WS.receiveData conn
          case readMaybe $ T.unpack firstMsg
            of Nothing ->
                 WS.sendTextData conn ("Error: unknown channel" :: T.Text)
               Just channel ->
                 do clientID <- liftM2 (,) getCurrentTime randomIO
                    flip finally (disconnect channel clientID) $
                      do modifyMVar_ stateMVar . (return .) $
                           over (channelS channel . clientsCS)
                             (M.insert clientID conn)
                         whenJustM
                           (   preview
                                 ( channelS channel
                                 . lastMessageCS
                                 . traverse
                                 )
                           <$> readMVar stateMVar
                           )
                           ( WS.sendTextData conn )
                         (_ :: T.Text) <- WS.receiveData conn
                         WS.sendTextData conn
                           ("Error: unexpected message" :: T.Text)
                         -- and now the connection closes
  where
    disconnect channel clientID =
      modifyMVar_ stateMVar . (return .) $
        over (channelS channel . clientsCS) (M.delete clientID)

-- | The broadcast action.
broadcast :: forall c . HasProtocol c => MVar State -> MessageType c -> IO ()
broadcast stateMVar msg =
  do let msgText = toMessageData msg
         channel = reflectChannel @c
     clients <- foldOf (channelS channel . clientsCS) <$> readMVar stateMVar
     forM_ clients $ flip WS.sendTextData msgText
     modifyMVar_ stateMVar . (return .) $
       set (channelS channel . lastMessageCS) (Just msgText)

-- | Returns whether there are clients subscribed to the channel.
hasClients :: MVar State -> Channel -> IO Bool
hasClients stateMVar channel =
  notNullOf (channelS channel . clientsCS) <$> readMVar stateMVar
