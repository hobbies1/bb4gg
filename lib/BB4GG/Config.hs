{-# LANGUAGE TemplateHaskell #-}

module BB4GG.Config where

import BB4GG.Config.TH (makeConfigLenses)

import Conferer (DefaultConfig (..), FromConfig)
import Conferer.FromConfig.Snap ()
import Data.Time (NominalDiffTime)
import GHC.Generics (Generic)
import Snap.Core (Snap)
import Snap.Http.Server (Config)


-- We are deviating from our usual naming conventions here (that @Bb4gg@ is really burning my eyes...) in order to profit from generics-based implementation of 'FromConfig', cf. https://conferer.ludat.io/docs/multiple-tutorial

-- | Contains paths to the directories used by BB4GG.
data DirConfig = DirConfig
  { dirConfigData  :: FilePath -- ^ Directory with runtime data files.
  , dirConfigBb4gg :: FilePath -- ^ Directory contanining directories for static files and HTML templates.
  } deriving (Show, Generic)

makeConfigLenses ''DirConfig

instance FromConfig DirConfig

instance DefaultConfig DirConfig where
  configDef =
    DirConfig
      { dirConfigData  = "data"
      , dirConfigBb4gg = "."
      }

-- | Essentially a copy of 'InstanceConfiguration' using Conferer's naming conventions.
data NewInstanceConfig = NewInstanceConfig
  { newInstanceConfigListed                :: Bool
  , newInstanceConfigAssignmentsToCompute  :: Int
  , newInstanceConfigRequireReady          :: Maybe NominalDiffTime
  , newInstanceConfigAutoDeactivationHours :: Maybe Int
  } deriving (Eq, Show, Generic)

makeConfigLenses ''NewInstanceConfig

instance FromConfig NewInstanceConfig

instance DefaultConfig NewInstanceConfig where
  configDef =
    NewInstanceConfig
      { newInstanceConfigListed                = True
      , newInstanceConfigAssignmentsToCompute  = 10
      , newInstanceConfigRequireReady          = Nothing
      , newInstanceConfigAutoDeactivationHours = Just 12
      }

-- | Contains paths to the directories used by BB4GG.
data BggConfig = BggConfig
  { bggConfigCacheMax  :: Maybe Int -- ^ Maximum number of BGG items to cache.
  , bggConfigCacheDays :: Int -- ^ Time (in days) to cache items retrieved from BGG.
  , bggConfigMaxThingsToRequest :: Int -- ^ The maximum number of BGG things to request from the API at once.
  , bggConfigMinDelay :: NominalDiffTime -- ^ The minimum delay between requests to the BGG API.
  , bggConfigDelayIncreaseOn429 :: Double -- ^ By which factor the delay between requests should be increased on a 429 (Too Many Requests) response.
  , bggConfigDelayDecreasePerMin :: Double -- ^ By what factor the delay between requests should decrease per minute (down to 'minDelay').
  } deriving (Show, Generic)

makeConfigLenses ''BggConfig

instance FromConfig BggConfig

instance DefaultConfig BggConfig where
  configDef =
    BggConfig
      { bggConfigCacheMax = Nothing
      , bggConfigCacheDays = 7
      , bggConfigMaxThingsToRequest = 500
      , bggConfigMinDelay = 0.1
      , bggConfigDelayIncreaseOn429 = 5
      , bggConfigDelayDecreasePerMin = 0.5
      }

-- | Contains parameters that can be used to change the behavior of BB4GG.
data Bb4ggConfig = Bb4ggConfig
  { bb4ggConfigSnap     :: Config Snap ()
  , bb4ggConfigDirs     :: DirConfig
  , bb4ggConfigInstance :: NewInstanceConfig
  , bb4ggConfigBgg      :: BggConfig
  } deriving (Show, Generic)

makeConfigLenses ''Bb4ggConfig

instance FromConfig Bb4ggConfig

instance DefaultConfig Bb4ggConfig where
  configDef =
    Bb4ggConfig
      { bb4ggConfigSnap     = configDef
      , bb4ggConfigDirs     = configDef
      , bb4ggConfigInstance = configDef
      , bb4ggConfigBgg      = configDef
      }
