module BB4GG.Config.TH (makeConfigLenses) where

import Control.Lens ((&), (.~))
import Control.Lens.TH (DefName(..), makeLensesWith, lensField, lensRules)
import Data.Char (isUpper, toLower)
import Language.Haskell.TH (DecsQ, Name, mkName, nameBase)

-- | Generates lenses for configuration types (whose field names don't follow the usual conventions because of Conferer's automatic instance derivation mechanisms).
--
-- The names of the lenses are computed as follows: We drop the type name prefix from the field name, transform the first character that remains to lower case, and append all the upper case letters in the type name to the result.
-- This is similar to what the @camelCaseNamer@ of the lens package does, but manually implementing it seemed easier than trying to manipulate the result of @camelCaseNamer@.
makeConfigLenses :: Name -> DecsQ
makeConfigLenses =
  makeLensesWith $
    lensRules
      &  lensField
      .~ ( \ typeName _ fieldName ->
             [ TopName . mkName $
                 let t = nameBase typeName
                     l = length t
                     f = nameBase fieldName
                 in toLower (f !! l) : drop (l + 1) f ++ filter isUpper t
             ]
         )
