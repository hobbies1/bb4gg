{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -Wno-redundant-constraints #-}

module BB4GG.Util.Timestamped ( Timestamped
                              , stamp
                              , stampIO
                              , timestampTS
                              , touch
                              , touchIO
                              , valueStampedTS
                              , valueTS
                              , valueUnstampedTS
                              ) where

import Data.Aeson.TH (defaultOptions, deriveJSON, fieldLabelModifier)
import Data.SafeCopy (base, deriveSafeCopy)
import Control.Lens ( Getter
                    , Setter'
                    , iso
                    , makeLensesFor
                    , set
                    , to
                    )
import Data.Time (UTCTime, getCurrentTime)


-- | Represents a value with a timestamp.
-- Read access for the value is via 'valueTS', write acces via 'valueStampedTS' if the timestamp should be updated, or 'valueUnstampedTS' if it should not.
data Timestamped a = Timestamped
  { _timestampTS :: UTCTime
  , _valueTS :: a
  } deriving (Eq, Show)

deriveSafeCopy 1 'base ''Timestamped
$(deriveJSON defaultOptions{fieldLabelModifier = drop 1} ''Timestamped)

makeLensesFor
  [("_timestampTS", "timestampTS"), ("_valueTS", "valueUnstampedTS")]
  ''Timestamped

-- Read access to the value contained in 'Timestamped a'.
valueTS :: Getter (Timestamped a) a
valueTS = to _valueTS

-- Write access to the value contained in 'Timestamped a', updating the timestamp.
-- Note: this does not actually yield a legal Setter' (though it has the correct type), since it does not fulfill that "over (valueStampedTS t) id == id"; the left hand side is equal to "set timestamp t" instead.
valueStampedTS :: UTCTime -> Setter' (Timestamped a) a
valueStampedTS t = iso _valueTS $ Timestamped t

-- Timestamp a value.
stamp :: UTCTime -> a -> Timestamped a
stamp = Timestamped

-- Timestamp a value.
stampIO :: a -> IO (Timestamped a)
stampIO a = fmap (`stamp` a) getCurrentTime

-- Update the timestamp.
touch :: UTCTime -> Timestamped a -> Timestamped a
touch = set timestampTS

-- Update the timestamp.
touchIO :: Timestamped a -> IO (Timestamped a)
touchIO a = fmap (`touch` a) getCurrentTime
