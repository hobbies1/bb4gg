{-# LANGUAGE UndecidableInstances #-}

module BB4GG.Util.Monad.HeistState
  ( BB4GGHeistM
  , MonadHeistState (..)
  , renderWithHeistState
  , renderWithSplices
  , respondWithHeistState
  , respondWithSplices
  , respondWithSplices'
  ) where

import Control.Arrow (first)
import Control.Monad.IO.Class (MonadIO)
import Control.Monad.Trans (MonadTrans, lift, liftIO)
import Data.ByteString.Builder (toLazyByteString)
import Heist (AttrSplice, HeistState, MIMEType, Splices)
import Heist.Interpreted (Splice, bindSplices, renderTemplate, bindAttributeSplices)
import Snap.Core ( MonadSnap (..)
                 , addHeader
                 , modifyResponse
                 , setResponseCode
                 , writeLBS
                 )

import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL


-- | The "runtime monad" for Heist.
--
-- Using Heist with digestive functors requires a 'MonadIO'.
-- For now,  we hard code the 'IO' here, but one cold use a more sophisticated monad in future.
type BB4GGHeistM = IO

-- | This class describes the functionality required to render web pages using Heist.
class MonadIO m => MonadHeistState m where
  -- | Access the global heist state.
  obtainHeistState :: m (HeistState BB4GGHeistM)

-- | The following functions render Heist templates using the obtained 'HeistState'.
-- They are intended to be used only when sending complete pages over WebSockets (which is a questionable practice and might be removed in future along with these functions).
-- For cooking up HTTP responses, see the functions that come later.

-- | Rendering the template using an identifier and some local modifcation of the 'HeistState'.
renderWithHeistState ::
     MonadHeistState m
  => BS.ByteString                                      -- ^ Template identifier
  -> (HeistState BB4GGHeistM -> HeistState BB4GGHeistM) -- ^ The function to apply to the global state before rendering this template
  -> m (Maybe (BSL.ByteString, MIMEType))
renderWithHeistState template stateF =
  do heistState <- stateF <$> obtainHeistState
     liftIO $
       fmap (first toLazyByteString) <$> renderTemplate heistState template

-- | Rendering the template using an identifier and non-default splices.
renderWithSplices ::
     MonadHeistState m
  => BS.ByteString                -- ^ Template identifier
  -> Splices (Splice BB4GGHeistM) -- ^ Additional splices to use
  -> m (Maybe (BSL.ByteString, MIMEType))
renderWithSplices template splices =
  renderWithHeistState template $ bindSplices splices

-- | Next, we have the functions which render a Heist template as a response in a 'MonadSnap'.
-- The "idiomatic" way of doing this would probably be using the corresponding 'Snaplet', but switching to a "Snaplet architecture" is a consideration for another day.

-- | Rendering an HTTP response using an identifier and some local modification of the 'HeistState'.
respondWithHeistState ::
  (MonadSnap m, MonadHeistState m)
  => BS.ByteString                                      -- ^ Template identifier
  -> (HeistState BB4GGHeistM -> HeistState BB4GGHeistM) -- ^ The function to apply to the global state before rendering this template
  -> m ()
respondWithHeistState template stateF =
  do rendered <- renderWithHeistState template stateF
     case rendered
       of Nothing ->
            modifyResponse (setResponseCode 500)
              >> writeLBS "Internal server error: Could not render template."
          Just (byteString, mimeType) ->
            modifyResponse
              (addHeader "Content-Type" $ mimeType <> "; charset=UTF-8")
               >> writeLBS byteString

-- | Rendering an HTTP response using an identifier and non-default splices.
respondWithSplices ::
  (MonadSnap m, MonadHeistState m)
  => BS.ByteString                -- ^ Template identifier
  -> Splices (Splice BB4GGHeistM) -- ^ Additional splices to use
  -> m ()
respondWithSplices template splices =
  respondWithSplices' template splices mempty

-- | Rendering an HTTP response using an identifier and non-default splices.
respondWithSplices' ::
  (MonadSnap m, MonadHeistState m)
  => BS.ByteString                -- ^ Template identifier
  -> Splices (Splice BB4GGHeistM) -- ^ Additional splices to use
  -> Splices (AttrSplice BB4GGHeistM) -- ^ Additional attribute splices to use
  -> m ()
respondWithSplices' template splices attrSplices =
  respondWithHeistState template $
    bindSplices splices . bindAttributeSplices attrSplices

instance {-# OVERLAPPABLE #-}
  ( MonadHeistState m
  , MonadIO (t m)
  , MonadTrans t
  ) => MonadHeistState (t m)
  where
  obtainHeistState = lift obtainHeistState
