{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE QuantifiedConstraints #-}
{-# LANGUAGE UndecidableInstances #-}

module BB4GG.Util.Monad.Unlockable
  ( LiftsThrough
  , LiftsThrough2
  , LiftsThroughUnlockT
  , MonadUnlockable (..)
  , MonadUnlockable'
  , MonadUnlockableLE
  , MonadUnlockableLT
  , UnlockT
  , UnlockT'
  , runUnlocked
  , runUnlocked'
  , runUnlockedOrNotVoid
  , runUnlockedOrNotWith
  , runUnlockedVoid
  ) where

import Control.Applicative (Alternative)
import Control.Concurrent (ThreadId, myThreadId)
import Control.Concurrent.STM ( TMVar
                              , atomically
                              , putTMVar
                              , tryReadTMVar
                              , tryTakeTMVar
                              )
import Control.Exception (bracket)
import Control.Monad (MonadPlus, when)
import Control.Monad.Base (MonadBase)
import Control.Monad.Except (MonadError)
import Control.Monad.Extra (unlessM)
import Control.Monad.Identity (IdentityT (..), runIdentityT)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Morph (MFunctor, hoist)
import Control.Monad.State (MonadState (..))
import Control.Monad.Trans (MonadTrans, lift)
import Control.Monad.Trans.Control ( ComposeSt
                                   , MonadBaseControl (..)
                                   , MonadTransControl (..)
                                   , defaultLiftBaseWith
                                   , defaultLiftWith
                                   , defaultRestoreM
                                   , defaultRestoreT
                                   , liftBaseOp
                                   )
import Data.Maybe (fromJust, fromMaybe)
import Data.Kind (Constraint)
import GHC.TypeNats (Nat, type (-))
import Snap.Core (MonadSnap (..))


-- | This is an identity monad transformer qualified over a type level natural (indicating a "lock level") for use in the 'MonadUnlockable' class.
-- See there for more documentation.
newtype UnlockT (l :: Nat) m a = UnlockT
  { idUT :: IdentityT m a
  } deriving
      ( Alternative
      , Applicative
      , Functor
      , MFunctor
      , Monad
      , MonadBase b
      , MonadError e
      , MonadIO
      , MonadPlus
      , MonadTrans
      )

-- | A type synonym for @'UnlockT' 0@ in the case that there is only one lock level.
type UnlockT' = UnlockT 0

-- | Unpacking the identity transformer.
-- Only for internal use!
runUnlockT :: UnlockT l m a -> m a
runUnlockT = runIdentityT . idUT

-- | Necessary for default 'MonadBaseControl' instance.
-- Can not be derived because of the associated type family.
instance MonadTransControl (UnlockT l) where
  type StT (UnlockT l) a = StT IdentityT a
  liftWith           = defaultLiftWith UnlockT idUT
  restoreT           = defaultRestoreT UnlockT

-- | Necessary for 'MonadSnap' instance.
-- Can not be derived because of the associated type family.
instance MonadBaseControl b m => MonadBaseControl b (UnlockT l m) where
  type StM (UnlockT l m) a = ComposeSt (UnlockT l) m a
  liftBaseWith           = defaultLiftBaseWith
  restoreM               = defaultRestoreM

instance MonadSnap m => MonadSnap (UnlockT l m) where
  liftSnap = lift . liftSnap

-- | Lift a 'MonadState' instance through a monad transformer applied on the inside of 'UnlockT'.
-- This is possible since 'UnlockT' is just the identity.
instance {-# OVERLAPPABLE #-}
  (Monad m, MonadTrans t, Monad (t m), MonadState s (UnlockT l m))
    => MonadState s (UnlockT l (t m)) where
  get = hoist lift get
  put = hoist lift . put
  state = hoist lift . state

-- | This is a constraint signifying that the constraint @c@ can be lifted through the transformer @t@.
type LiftsThrough t c = (forall m . c m => c (t m) :: Constraint)

-- | This is a constraint signifying that the constraint @c@ can be lifted through the transformer @t a@ whenever @a@ fulfills the constraint @d@.
type LiftsThrough2 t d c = (forall m a . (c m, d a) => c (t a m) :: Constraint)

-- | This is a constraint signifying that the constraint @c@ can be lifted through the transformer @'UnlockT' l@ for all type level naturals @l@.
type LiftsThroughUnlockT c = forall (l :: Nat) . LiftsThrough (UnlockT l) c

-- | This is a type class that facilitates the acquiring and releasing of locks (for multi threaded applications).
-- A monad @m@ which instantiates @'MonadUnlockable' l c@ for some type level natural @l@ and constraint @c@ provides a lock level @l@.
-- This allows emedding an @'UnlockT' l@ action into the monad @m@.
-- This will take care (via the implementation provided in 'runUnlockedOrNotWith') of acquiring the correct lock before executing the action and releasing it afterwards (or in case of an error).
-- Via a forall quantification the only functionality provided in the embedded @'UnlockT' l@ is the one provided by the constraint @c@ in addition to lock levels at most @l@.
-- This guarantees that no deadlocks can occur (if thread A has acquired lock @0@ and thread B lock @1@, then it is impossible for thread A to try and acquire lock @1@, so they can't deadlock each other).
-- A monad which instantiates @'MonadUnlockable' l c@ also needs to instantiate @'MonadUnlockable' k c@ for all type level naturals @k@ smaller than @l@.
-- Note that using this with a constraint @c@ which implies @'MonadUnlockable' l d@ for some @l@ and @d@ breaks the guarantees provided by this.
class
  ( Monad m
  , LiftsThroughUnlockT c
  ) => MonadUnlockable (l :: Nat) c m | m -> c
  where
  runUnlockedOrNot
    :: Bool -- ^ Whether to wait for the lock to be free, or abort if it is taken.
    -> (forall n . (c n, MonadUnlockableLE l c n) => UnlockT l n a) -- ^ The action to execute
    -> m (Maybe a) -- ^ Only returns 'Nothing' if the lock was already taken and the action was aborted.

-- | A constraint that is equivalent to the intersection of @'MonadUnlockable' k@ for all type level naturals @k@ smaller than @l@.
type family MonadUnlockableLT l c m :: Constraint where
  MonadUnlockableLT 0 c m = ()
  MonadUnlockableLT l c m =
    (MonadUnlockable (l - 1) c m, MonadUnlockableLT (l - 1) c m)

-- | A constraint that is equivalent to the intersection of @'MonadUnlockable' k@ for all type level naturals @k@ smaller or equal to @l@.
type MonadUnlockableLE l c m = (MonadUnlockable l c m, MonadUnlockableLT l c m)

-- | A constraint synonym for @'MonadUnlockable' 0 c@ in the case that there is only one lock level.
type MonadUnlockable' c = MonadUnlockable 0 c

-- | A synonym for 'runUnlockedOrNot' for voided actions.
runUnlockedOrNotVoid
  :: forall (l :: Nat) c m
  .  MonadUnlockable l c m
  => Bool
  -> (forall n . (c n, MonadUnlockableLE l c n) => UnlockT l n ())
  -> m (Maybe ())
runUnlockedOrNotVoid = runUnlockedOrNot @ l

-- | A version of 'runUnlockedOrNot' that always waits for the lock to be free.
runUnlocked
  :: forall (l :: Nat) c m a
  .  MonadUnlockable l c m
  => (forall n . (c n, MonadUnlockableLE l c n) => UnlockT l n a)
  -> m a
runUnlocked ut = fromJust <$> runUnlockedOrNot @ l True ut

-- | A synonym for 'runUnlocked' in the case that there is only one lock level.
runUnlocked'
  :: forall c m a
  .  MonadUnlockable 0 c m
  => (forall n . (c n, MonadUnlockableLE 0 c n) => UnlockT' n a)
  -> m a
runUnlocked' = runUnlocked @ 0

-- | A synonym for 'runUnlocked' for voided actions.
runUnlockedVoid
  :: forall (l :: Nat) c m
  .  MonadUnlockable l c m
  => (forall n . (c n, MonadUnlockableLE l c n) => UnlockT l n ())
  -> m ()
runUnlockedVoid = runUnlocked @ l

instance {-# OVERLAPPABLE #-}
  ( MonadUnlockable l c m
  , MonadTrans t
  , Monad (t m)
  ) => MonadUnlockable l c (t m)
  where
  runUnlockedOrNot wait ut = lift $ runUnlockedOrNot @ l wait ut

-- | The default implementation of 'runUnlocked' (and the only one that can be used, since no other way to unpack 'UnlockT' is exported).
-- The function is provided with a @m ('TMVar' 'ThreadId')@ which is the lock to be used.
-- It will check whether the thread already has the lock acquired in which case the action is simply exexuted.
-- If the lock is taken by another thread and the passed bool is false, nothing is done and 'Nothing' is returned.
-- Otherwise the lock is acquired (or waited until it free), the provided action is executed, and the lock is released again.
-- This is done in an exception safe manner via a @'MonadBaseControl' 'IO'@ instance.
runUnlockedOrNotWith
  :: forall l c m a
  .  (MonadIO m, MonadBaseControl IO m, c m, MonadUnlockableLE l c m)
  => m (TMVar ThreadId)
  -> Bool
  -> (forall n . (c n, MonadUnlockableLE l c n) => UnlockT l n a)
  -> m (Maybe a)
runUnlockedOrNotWith lockM wait ut =
  do lockTMVar <- lockM
     threadId <- liftIO myThreadId
     liftBaseOp
       ( bracket
           ( atomically $
               do mAlreadyHaveLock <-
                    fmap (== threadId) <$> tryReadTMVar lockTMVar
                  let acquireLock =
                        maybe True (\ have -> wait && not have) mAlreadyHaveLock
                  when acquireLock $ putTMVar lockTMVar threadId
                  return (fromMaybe True mAlreadyHaveLock, acquireLock)
           )
           ( \ (_, acquiredLock) ->
               when acquiredLock $
                 unlessM
                   ( fmap (== Just threadId) . atomically $
                       tryTakeTMVar lockTMVar
                   )
                   (error "Lock unexpectedly released or taken")
           )
       )
       ( \ (alreadyHadLock, acquiredLock) ->
           if acquiredLock || alreadyHadLock
             then fmap Just (runUnlockT ut)
             else return Nothing
       )
