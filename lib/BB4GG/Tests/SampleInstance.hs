module BB4GG.Tests.SampleInstance (addSampleInstance) where

import BB4GG.Core

import BB4GG.Tests.GameData (games)
import BB4GG.Tests.RandomPlayerData (randomPlayers)

import Conferer (DefaultConfig (..))
import Control.Lens ((&), (.~))
import Control.Monad (replicateM)
import Control.Monad.Extra (untilJustM)
import Control.Monad.IO.Class (MonadIO (..))
import Control.Monad.Random.Lazy (randomRIO)
import Control.Monad.State ()
import Data.Time (getCurrentTime)

import qualified Data.Set as S


-- | Replaces the state of an instance and saves it.
replaceInstanceState ::
  MonadBB4GGInstance LockISt MonadIO m => InstanceState -> m ()
replaceInstanceState is =
  runUnlocked @LockISt (put is) >> saveInstanceStateUnlocked

-- | Creates a sample instance with the games from 'BB4GG.Test.GameData' and the given number of "random" players.
addSampleInstance :: MonadBB4GG MonadIO m => Int -> m ()
addSampleInstance num =
  do name <- runUnlocked' $
       do names <- getInstanceNames
          name <- untilJustM . liftIO $
            do name' <- ("Sample" ++) <$> replicateM 8 (randomRIO ('A', 'Z'))
               if name' `S.member` names
                 then return Nothing
                 else return (Just name')
          newInstance name
          return name
     now <- liftIO getCurrentTime
     players <- liftIO $ randomPlayers num games
     let is =
           (initialInstanceState now . toInstanceConfiguration $ configDef)
             & gamesIS .~ games & playersIS .~ players
     _ <- runInstance (replaceInstanceState is) name
     liftIO . putStrLn $ "Created sample instance " ++ name
