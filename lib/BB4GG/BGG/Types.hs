{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveAnyClass #-}

module BB4GG.BGG.Types where

import Control.DeepSeq (NFData)
import Control.Exception (SomeException)
import Control.Lens (makeLenses, makePrisms)
import Data.String (IsString)
import GHC.Generics (Generic)
import Network.HTTP.Client (HttpExceptionContent)

import qualified Data.Set as  S
import qualified Data.Text as T


-- | Identifier for BGG items.
type BGGID = Int

type BGGLinkType = T.Text

-- | Link with an other BGG item (e.g. an expansion or a boardgamecategory).
data BGGLink = BGGLink
  { _idBGGL :: BGGID
  , _typeBGGL :: BGGLinkType
  , _nameBGGL :: T.Text
  } deriving (Eq, Generic, NFData, Ord, Show)

makeLenses ''BGGLink

-- | The type of an item from BGG.
data BGGThingType
  = BoardgameBGGTT
  | BoardgameexpansionBGGTT
  | BoardgameaccessoryBGGTT
  | VideogameBGGTT
  | RpgitemBGGTT
  | RpgissueBGGTT
  deriving (Eq, Generic, NFData, Ord, Show)

makePrisms ''BGGThingType

showBGGThingType :: IsString s => BGGThingType -> s
showBGGThingType BoardgameBGGTT = "boardgame"
showBGGThingType BoardgameexpansionBGGTT = "boardgameexpansion"
showBGGThingType BoardgameaccessoryBGGTT = "boardgameaccessory"
showBGGThingType VideogameBGGTT = "videogame"
showBGGThingType RpgitemBGGTT = "rpgitem"
showBGGThingType RpgissueBGGTT = "rpgissue"

readMaybeBGGThingType :: (Eq s, IsString s) => s -> Maybe BGGThingType
readMaybeBGGThingType "boardgame" = Just BoardgameBGGTT
readMaybeBGGThingType "boardgameexpansion" = Just BoardgameexpansionBGGTT
readMaybeBGGThingType "boardgameaccessory" = Just BoardgameaccessoryBGGTT
readMaybeBGGThingType "videogame" = Just VideogameBGGTT
readMaybeBGGThingType "rpgitem" = Just RpgitemBGGTT
readMaybeBGGThingType "rpgissue" = Just RpgissueBGGTT
readMaybeBGGThingType _ = Nothing

-- | Some of the data about a game provided by BGG.
data BGGThing = BGGThing
  { _idBGGT :: BGGID
  , _nameBGGT :: T.Text
  , _typeBGGT :: BGGThingType
  , _minPlayersBGGT :: Maybe Int
  , _maxPlayersBGGT :: Maybe Int
  , _recommendedPlayersBGGT :: S.Set Int
  , _yearBGGT :: Maybe Int
  , _minPlayTimeBGGT :: Maybe Int
  , _maxPlayTimeBGGT :: Maybe Int
  , _thumbnailURLBGGT :: Maybe T.Text
  , _imageURLBGGT :: Maybe T.Text
  , _descriptionBGGT :: T.Text
  , _linksBGGT :: [BGGLink]
  , _rankBGGT :: Maybe Int
  , _ratingBGGT :: Maybe Double
  , _weightBGGT :: Maybe Double
  } deriving (Eq, Generic, NFData, Ord, Show)

makeLenses ''BGGThing

-- | A name can either be primary or alternate.
data BGGNameType = PrimaryBGGNT | AlternateBGGNT deriving (Eq, Ord, Show)

makePrisms ''BGGNameType

readMaybeBGGNameType :: (Eq s, IsString s) => s -> Maybe BGGNameType
readMaybeBGGNameType "primary" = Just PrimaryBGGNT
readMaybeBGGNameType "alternate" = Just AlternateBGGNT
readMaybeBGGNameType _ = Nothing

-- | The result of a BGG search.
data BGGSearchResult = BGGSearchResult
  { _idBGGSR :: BGGID
  , _typeBGGSR :: BGGThingType
  , _nameBGGSR :: T.Text
  , _nameTypeBGSSR :: BGGNameType
  , _yearBGGSR :: Maybe Int
  } deriving (Eq, Ord, Show)

makeLenses ''BGGSearchResult

-- | The problems potentially occurring while doing BGG stuff.
data BGGException
  = BGGXMLException XMLException
  | BGGParseException BGGParseException
  | BGGHttpException HttpExceptionContent
  deriving (Show)

-- | The type of exceptions raised by 'XML.parseLBS'.
type XMLException = SomeException

-- | The problems that can occur while parsing something.
data BGGParseException
  = BGGParseNoIDException
  | BGGParseNoTypeException
  | BGGParseNoNameException
  | BGGParseNoNameTypeException
  deriving (Generic, NFData, Show)
