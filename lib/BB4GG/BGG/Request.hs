{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE UndecidableInstances #-}

module BB4GG.BGG.Request ( BGGState
                         , BGGT
                         , MonadBGG (embedBGGT, retrieveBGGThings)
                         , initBGG
                         , requestBGGSearch
                         , retrieveBGGThing
                         , runBGGT
                         ) where

import BB4GG.BGG.Types
import BB4GG.Config
import BB4GG.Util.Timestamped

import BB4GG.BGG.Parse (parseBGGSearch, parseBGGThings)
import BB4GG.Util.Cache (Cache, mkCache, retrieveMultiple)
import BB4GG.Util.Monad.HeistState (MonadHeistState)

import Control.Applicative (Alternative)
import Control.Concurrent.Forkable (ForkableMonad)
import Control.Concurrent.STM ( TVar
                              , TMVar
                              , atomically
                              , modifyTVar'
                              , newTMVarIO
                              , newTVarIO
                              , readTVarIO
                              , takeTMVar
                              , tryPutTMVar
                              )
import Control.Exception (catch, throw)
import Control.Lens ((^.), view, makeLenses)
import Control.Monad (MonadPlus, join, void)
import Control.Monad.Base (MonadBase)
import Control.Monad.Extra (mconcatMapM, unlessM)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Morph (MFunctor)
import Control.Monad.Reader (ReaderT, asks, runReaderT)
import Control.Monad.Trans (MonadTrans, lift)
import Control.Monad.Trans.Control (MonadBaseControl)
import Control.Concurrent (forkIO, threadDelay)
import Data.Bifunctor (bimap)
import Data.Either (rights)
import Data.Either.Extra (mapLeft)
import Data.List (intercalate)
import Data.List.Extra (chunksOf)
import Data.Time (NominalDiffTime, UTCTime, diffUTCTime, getCurrentTime)
import Network.HTTP.Client ( HttpException(..)
                           , HttpExceptionContent(StatusCodeException)
                           , responseStatus
                           )
import Network.HTTP.Types (statusCode)
import Network.Wreq (responseBody)
import Network.Wreq.Session (Session, get, newAPISession)
import Snap.Core (MonadSnap)


import qualified Data.ByteString.Lazy as BS
import qualified Data.Map.Strict as M
import qualified Data.Set as S


-- | Provides the ability to fetch items from the BGG API.
class MonadIO m => MonadBGG m where
  -- | Sends a request to the BGG API.
  -- This should handle a 429 response (Too Many Requests), and return 'Nothing' on any other error.
  requestBGG :: String -> m (Either HttpExceptionContent BS.ByteString)
  -- | Tries to fetch the items belonging to the given ids.
  retrieveBGGThings
    :: S.Set BGGID -> m (M.Map BGGID (Either BGGException BGGThing))
  -- | Needed for lifting from 'ForkableMonad'.
  embedBGGT :: BGGT n a -> m (n a)

instance {-# OVERLAPPABLE #-}
  ( MonadBGG m
  , MonadTrans t
  , MonadIO (t m)
  ) => MonadBGG (t m)
  where
  retrieveBGGThings = lift . retrieveBGGThings
  requestBGG = lift . requestBGG
  embedBGGT = lift . embedBGGT

-- | Combines 'requestBGG' and 'parseBGGSearch'.
-- URL special characters in the search query are ignored.
requestBGGSearch
  :: MonadBGG m
  => Bool               -- ^ Whether the search should only yield exact hits.
  -> S.Set BGGThingType -- ^ The thing types to search for
  -> String             -- ^ The search query.
  -> m (Either BGGException [BGGSearchResult])
requestBGGSearch _ types _ | S.null types = return $ Right []
requestBGGSearch exact types query =
  fmap parse . requestBGG $
       "search"
    ++ "?type=" ++ (intercalate "," . map showBGGThingType $ S.toList types)
    ++ (if exact then "&exact=1" else "")
    ++ "&query=" ++ query'
  where
    query' = filter (not . (`elem` ("$&+,/:;=?@#%" :: String))) query
    parse =
        join
      . bimap BGGHttpException (bimap BGGXMLException rights . parseBGGSearch)

-- | This is 'retrieveBGGThings' for a single game.
retrieveBGGThing
  :: MonadBGG m => BGGID -> m (Either BGGException (Maybe BGGThing))
retrieveBGGThing i = sequence . M.lookup i <$> retrieveBGGThings (S.singleton i)


-- | A monad transformer that adds BGG caching and rate control functionality.
newtype BGGT m a = BGGT
  { readerBGGT :: ReaderT BGGState m a
  } deriving
      ( Alternative
      , Applicative
      , ForkableMonad
      , Functor
      , MFunctor
      , Monad
      , MonadBase b
      , MonadBaseControl b
      , MonadHeistState
      , MonadIO
      , MonadPlus
      , MonadSnap
      , MonadTrans
      )

-- | Execute a 'BGGT'.
runBGGT :: BGGT m a -> BGGState -> m a
runBGGT = runReaderT . readerBGGT

-- | The state of the interaction with BGG.
data BGGState = BGGState
  { -- | A cache for items from BGG.
    _cacheBGGT :: TVar (Cache BGGID BGGThing)
  , -- | The delay between requests requests currently used.
    -- This also functions as a lock - if it's full, a request can be made.
    _delayBGGT :: TMVar (Timestamped NominalDiffTime)
  , -- ^ For handling the connection, in particular it can reuse TCP connections.
    _sessionBGGT :: Session
  , -- ^ The config.
    _configBGGT :: BggConfig
  }

makeLenses ''BGGState

-- | Initializes the 'BGGState' using the given configuration.
initBGG :: MonadIO m => BggConfig -> m BGGState
initBGG conf =
  do cacheVar <-
       liftIO . newTVarIO $
         mkCache
           (conf ^. cacheMaxBC)
           (fromIntegral $ (conf ^. cacheDaysBC) * 24 * 60 * 60)
     delayVar <- liftIO $ newTMVarIO =<< stampIO (conf ^. minDelayBC)
     session <- liftIO newAPISession
     return $
       BGGState
         { _cacheBGGT = cacheVar
         , _delayBGGT = delayVar
         , _sessionBGGT = session
         , _configBGGT = conf
         }

-- Sends a GET request to the BGG API.
-- Only for internal use.
getBGG
  :: MonadIO m => String -> BGGT m (Either HttpExceptionContent BS.ByteString)
getBGG query =
  do session <- BGGT $ view sessionBGGT
     liftIO $ (Right . view responseBody <$> get session url) `catch` handler
  where
    url = "https://boardgamegeek.com/xmlapi2/" ++ query
    handler (HttpExceptionRequest _ hec) = return $ Left hec
    handler e@(InvalidUrlException _ _) = throw e


-- | Combines 'requestBGG' and 'parseBGGThings'.
-- This does not provide any caching; only for internal use!
-- Use 'retrieveBGGThings' instead.
requestBGGThings
  :: MonadIO m
  => S.Set BGGID
  -> BGGT m (M.Map BGGID (Either BGGException BGGThing))
requestBGGThings ids =
  do conf <- BGGT $ view configBGGT
     mconcatMapM requestBGGThings'
       . chunksOf (conf ^. maxThingsToRequestBC)
       $ S.toList ids
  where
    requestBGGThings' idChunk =
      either
        (allFailed . BGGHttpException)
        ( either
            (allFailed . BGGXMLException)
            (M.map $ mapLeft BGGParseException)
        . parseBGGThings
        )
        <$> requestBGG
              ("thing?stats=1&id=" ++ intercalate "," (show <$> idChunk))
      where
        allFailed e = M.fromList $ map (, Left e) idChunk

-- | Computes the current delay of requests from the current time and the previous delay.
currentDelay
  :: BggConfig -> UTCTime -> Timestamped NominalDiffTime -> NominalDiffTime
currentDelay conf now delayStamped =
  max (conf ^. minDelayBC) $ (delayStamped ^. valueTS) * realToFrac factor
  where
    timeDiff = now `diffUTCTime` (delayStamped ^. timestampTS)
    factor :: Double
    factor = (conf ^. delayDecreasePerMinBC) ** realToFrac (timeDiff / 60)

instance MonadIO m => MonadBGG (BGGT m) where
  retrieveBGGThings ids =
    do cacheVar <- BGGT $ view cacheBGGT
       cache <- liftIO $ readTVarIO cacheVar
       (results, cacheUpdate) <- retrieveMultiple ids requestBGGThings cache
       liftIO . atomically $ modifyTVar' cacheVar cacheUpdate
       return results
  requestBGG query =
    do delayVar <- BGGT $ view delayBGGT
       conf <- BGGT $ view configBGGT
       now <- liftIO getCurrentTime
       delay <- liftIO $
         currentDelay conf now <$> atomically (takeTMVar delayVar)
       (result, newDelay) <- makeRequest conf delay
       void . liftIO . forkIO $
         do threadDelay $ toMicro newDelay
            unlessM (atomically . tryPutTMVar delayVar $ stamp now newDelay) $
              error "BGG delay lock unexpectedly released"
       return result
    where
      toMicro = round . (* 1000000)
      makeRequest conf delay = getBGG query >>=
        \case
          Left (StatusCodeException r _)
            | statusCode (responseStatus r) == 429 ->
            do let newDelay = realToFrac (conf ^. delayIncreaseOn429BC) * delay
               liftIO . threadDelay $ toMicro newDelay
               makeRequest conf newDelay
          x -> return (x, delay)
  embedBGGT bt = BGGT . asks $ runBGGT bt
