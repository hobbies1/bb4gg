module BB4GG.BGG.Extract (updateGamesWithBGG) where

import BB4GG.Types

import BB4GG.BGG.Request (MonadBGG, requestBGGSearch, retrieveBGGThings)
import BB4GG.Util (clamp)

import Control.Lens ((^.), set, view)
import Control.Monad ((<=<))
import Data.Either.Extra (eitherToMaybe)
import Data.Maybe (isNothing)

import qualified Data.Map.Strict as M
import qualified Data.Set as S


-- | Returns a function that adds the BGG id of each game if it is missing and searching for its name yields only a single exact hit (and the game wasn't modified in the meantime).
updateGamesWithBGG
  :: MonadBGG m
  => Bool -- ^ Whether to also set the recommended player numbers.
  -> S.Set BGGThingType -- ^ The types to search for.
  -> Games
  -> m (Games -> Games)
updateGamesWithBGG setRecom types games
  | M.null toSearch || S.null types = return id
  | otherwise =
  do searchResults <- traverse (doSearch . view nameGD) toSearch
     let uniqueHits =
           M.mapMaybe (fmap (view idBGGSR) . single <=< eitherToMaybe)
             searchResults
     bggGames <-
         fmap (M.mapMaybe eitherToMaybe)
       . retrieveBGGThings
       . S.fromList
       $ M.elems uniqueHits
     let bggGames' = M.mapMaybe (bggGames M.!?) uniqueHits
     return $ \ games' ->
       M.foldrWithKey
         ( \ gID bggGame gs ->
             M.adjustWithKey (updateWithBGGGame bggGame)
               gID
               gs
         )
         games'
         bggGames'
  where
    toSearch = M.filter (isNothing . view bggIDGD) games
    single [a] = Just a
    single _ = Nothing
    doSearch = requestBGGSearch True types
    updateWithBGGGame bggGame gID gd' | gd' == games M.! gID =
      (if setRecom then transcribeRecom bggGame else id) $
        set bggIDGD (Just $ bggGame ^. idBGGT) gd'
    updateWithBGGGame _ _ gd = gd

-- | Transcribes the recommended player numbers from the BGG game.
transcribeRecom :: BGGThing -> GameData -> GameData
transcribeRecom bggGame gd =
  maybe id (set recommendedMinPlayersGD . clampPlayers)
    (S.lookupMin $ bggGame ^. recommendedPlayersBGGT)
    $ maybe id (set recommendedMaxPlayersGD . clampPlayers)
        (S.lookupMax $ bggGame ^. recommendedPlayersBGGT)
        gd
  where
    clampPlayers = clamp (gd ^. minPlayersGD, gd ^. maxPlayersGD)
