{-# OPTIONS_GHC -Wno-redundant-constraints #-}

module BB4GG.BGG.Parse (parseBGGSearch, parseBGGThings) where

import BB4GG.BGG.Types

import Control.Applicative ((<|>))
import Control.DeepSeq (force)
import Control.Lens (Fold, (^?), (^.), (^..), (...), _1, filtered, plate, to, toListOf)
import Data.Either.Extra (maybeToEither)
import Data.Functor ((<&>))
import Data.Time.Calendar (toGregorian)
import Data.Time.Format.ISO8601 (iso8601ParseM)
import Data.Set.Lens (setOf)
import Text.Read (readMaybe)
import Text.XML.Lens (attr, attributeIs, named, root, text)

import qualified Data.ByteString.Lazy as BS
import qualified Data.Map.Strict as M
import qualified Data.Text as T
import qualified Text.XML as XML


-- | Parses an XML list of things from the BGG API.
-- Throws an error if the XML can not be parsed.
-- An ID will be missing from the resulting 'Map' if it does not exist.
parseBGGThings
  :: BS.ByteString
  -> Either XMLException (M.Map BGGID (Either BGGParseException BGGThing))
parseBGGThings xml =
      force -- Force evaluation now
  .   M.fromList
  .   toListOf (root . named "items" ... named "item" . to parseItem . traverse)
  <$> eDoc
  where
    eDoc = XML.parseLBS XML.def xml
    parseItem item =
      (item ^? attr "id" . toReadMaybe) <&> \ gameID -> (gameID, ) $
        do thingType <-
             maybeToEither BGGParseNoTypeException $ 
               item ^? attr "type" . to readMaybeBGGThingType . traverse
           gameName <-
             maybeToEither BGGParseNoNameException $ 
               item
                 ^? plate
                 .  named "name"
                 .  attributeIs "type" "primary"
                 .  attr "value"
           return $
             BGGThing
               { _idBGGT = gameID
               , _nameBGGT = gameName
               , _typeBGGT = thingType
               , _minPlayersBGGT =
                   item ^? childValue "minplayers" . filtered (> 0)
               , _maxPlayersBGGT =
                   item ^? childValue "maxplayers" . filtered (> 0)
               , _recommendedPlayersBGGT = setOf recommendedPlayers item
               , _yearBGGT =
                       item ^? childValue "yearpublished" . filtered (/= 0)
                   <|> item
                         ^? plate
                         .  named "releasedate"
                         .  attr "value"
                         .  to (iso8601ParseM @ Maybe . T.unpack)
                         .  traverse
                         .  to toGregorian
                         .  _1
                         .  to fromIntegral
               , _minPlayTimeBGGT = item ^? childValue "minplaytime"
               , _maxPlayTimeBGGT = item ^? childValue "maxplaytime"
               , _thumbnailURLBGGT = item ^? childText "thumbnail"
               , _imageURLBGGT = item ^? childText "image"
               , _descriptionBGGT = item ^. childText "description"
               , _linksBGGT = item ^.. links
               , _rankBGGT = item ^? statRatings . rankType "subtype"
               , _ratingBGGT =
                   item ^? statRatings . childValue "average" . filtered (> 0)
               , _weightBGGT =
                   item
                     ^? statRatings
                     .  childValue "averageweight"
                     .  filtered (> 0)
               }
    childValue childName = plate . named childName . attr "value" . toReadMaybe
    childText childName = plate . named childName . text
    statRatings :: Fold XML.Element XML.Element
    statRatings = plate . named "statistics" ... named "ratings"
    rankType t =
      plate
        .   named "ranks"
        ... named "rank"
        .   attributeIs "type" t
        .   attr "value"
        .   toReadMaybe
    votes :: T.Text -> Fold XML.Element Int
    votes forWhat =
      plate
        . named "result"
        . attributeIs "value" forWhat
        . attr "numvotes"
        . toReadMaybe
    recommendedPlayers =
      plate
        .   named "poll"
        .   attributeIs "name" "suggested_numplayers"
        ... named "results"
        .   filtered
              ( \ r ->
                  -- This condition is a bit arbitrary; better suggestions are welcome.
                  ((+) <$> r ^? votes "Recommended" <*> r ^? votes "Best")
                    > fmap (3 *) (r ^? votes "Not Recommended")
              )
        .   attr "numplayers"
        .   toReadMaybe
    links :: Fold XML.Element BGGLink
    links =
      plate
        . named "link"
        . to (\ l ->
                do linkID <- l ^? attr "id" . toReadMaybe
                   linkType <- l ^? attr "type"
                   linkName <- l ^? attr "value"
                   return $
                     BGGLink
                       { _idBGGL = linkID 
                       , _typeBGGL = linkType
                       , _nameBGGL = linkName
                       }
             )
        . traverse

-- | Parses the XML result of a BGG API search.
-- Throws an error if the XML can not be parsed.
parseBGGSearch
  :: BS.ByteString
  -> Either XMLException [Either BGGParseException BGGSearchResult]
parseBGGSearch xml =
  map parseItem . toListOf (root . named "items" ... named "item") <$> eDoc
  where
    eDoc = XML.parseLBS XML.def xml
    parseItem item =
      do i <-
           maybeToEither BGGParseNoIDException $ item ^? attr "id" . toReadMaybe
         t <-
           maybeToEither BGGParseNoTypeException $
             item ^? attr "type" . to readMaybeBGGThingType . traverse
         name <-
           maybeToEither BGGParseNoNameException $
             item ^? plate . named "name" . attr "value"
         nameType <-
           maybeToEither BGGParseNoNameTypeException $ 
             item
               ^? plate
               .  named "name"
               .  attr "type"
               .  to (readMaybeBGGNameType . T.unpack)
               .  traverse
         return $
           BGGSearchResult
             { _idBGGSR = i
             , _typeBGGSR = t
             , _nameBGGSR = name
             , _nameTypeBGSSR = nameType
             , _yearBGGSR =
                 item
                   ^? plate . named "yearpublished" . attr "value" . toReadMaybe
             }

-- A lens for reading from Text.
toReadMaybe :: Read a => Fold T.Text a 
toReadMaybe = to (readMaybe . T.unpack) . traverse
