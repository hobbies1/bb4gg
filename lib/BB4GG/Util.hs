module BB4GG.Util ( batch
                  , clamp
                  , insertNew
                  , periodicAction
                  , sortMap
                  , toRoundSeconds
                  ) where

import Control.Concurrent.AlarmClock (setAlarm, newAlarmClock', setAlarmNow)
import Control.Monad.State (MonadState, State, runState, state)
import Data.List (sortOn)
import Data.Time (NominalDiffTime, addUTCTime)
import Data.Tuple (swap)

import qualified Data.Map.Strict as M


-- | Add a new element to a map.
insertNew :: (Ord k, Num k) => a -> M.Map k a -> M.Map k a
insertNew x m = M.insert (maybe 0 fst (M.lookupMax m) + 1) x m

-- | Create a list whose members are the results of applying the given function to the key-value pairs of the given map and which is ordered by the original values in the map.
sortMap :: (Ord k, Ord a) => ((k, a) -> b) -> M.Map k a -> [b]
sortMap f = map f . sortOn swap . M.toList

-- | Round 'NominalDiffTime' to number of seconds.
toRoundSeconds :: NominalDiffTime -> Integer
toRoundSeconds = round

-- | Executes an action periodically with given time interval.
periodicAction
  :: NominalDiffTime -- ^ Time interval.
  -> IO ()           -- ^ Action to execute periodically. 
  -> IO ()
periodicAction interval action =
  setAlarmNow =<<
    newAlarmClock' (\ ac t -> action >> setAlarm ac (interval `addUTCTime` t))

-- | Clamps the value into the given range.
clamp :: Ord a => (a, a) -> a -> a
clamp (low, high) = min high . max low

-- | A utility function to batch multiple state actions into a single call of 'state'.
-- This is useful if each call to 'state' is atomic.
batch :: MonadState s m => State s a -> m a
batch = state . runState
