{-# LANGUAGE TemplateHaskell #-}

module BB4GG.Core.Computation.MILP (computeBestAssignments) where

import BB4GG.Core.Computation
import BB4GG.Types

import BB4GG.Types.Util ( assignmentScore
                        , copiesInAssignment
                        , gamePreferenceScore
                        , hashAssignmentGame
                        , preferredGameIDs
                        , preferringPlayers
                        , waitingPlayers
                        )

import Control.Arrow ((&&&))
import Control.Lens ((^.), (^?), makeLenses, view)
import Control.Monad (forM_, guard, void)
import Control.Monad.IO.Class (liftIO)
import Data.LinearProgram ( Bounds (..)
                          , Constraint (..)
                          , Direction (Max)
                          , GLPOpts (..)
                          , LP (..)
                          , LPM
                          , VarKind (..)
                          , addObjective
                          , allVars
                          , equalTo'
                          , execLPM
                          , geq'
                          , glpSolveVars
                          , leq'
                          , leqTo'
                          , linCombination
                          , mipDefaults
                          , setDirection
                          , setVarKind
                          )
import Data.Either (isRight, lefts, rights)
import Data.Foldable (for_, traverse_)
import Data.List (transpose)
import Data.Maybe (fromMaybe)
import Data.Monoid (Sum (..))
import Data.Time.Clock.POSIX (getPOSIXTime)
import System.IO (hPutStrLn, stderr)

import qualified Data.Map as M
import qualified Data.Set as S


-- | Encodes a game together with player ranges.
-- Used as a variable in the linear program that computes the best assignment.
data GameRangeVar = GameRangeVar
  { _gameIDGPV     :: GameID
  , _minPlayersGPV :: Int
  , _maxPlayersGPV :: Int
  } deriving (Eq, Ord, Show)

makeLenses ''GameRangeVar

-- | Encodes a preference of a player.
-- Used as a variable in the linear program that computes the best assignment.
data PrefVar = PrefVar
  { _playerIDPV :: PlayerID
  , _gameRangeVarPV  :: GameRangeVar
  } deriving (Eq, Ord, Show)

makeLenses ''PrefVar

-- | The type of a variable used in the linear program.
type Var = Either GameRangeVar PrefVar

-- | The result type of the linear program, mapping variables to their values in an optimal solution.
type Result = M.Map Var Int


-- | The time limit for computing an assignment.
timeLimit :: Int
timeLimit = 60

-- | This computes the best 'Assignment's that are currently possible.
-- It tries to "populate" an empty 'MVar' in 'state ^. computationStatusIS' without blocking, and does nothing if the 'MVar' is not empty or does not exist.
computeBestAssignments ::
  MonadBB4GGComputation m => InstanceState -> m ()
computeBestAssignments state =
  forM_ (linearProgram state) $
    computeNextBestAssignments
      (state ^. playersIS)
      (state ^. configurationIS . assignmentsToComputeIC)

-- | Computes the best n solutions of the given linear program where n is the given integer and updates the 'Computation'.
computeNextBestAssignments ::
  MonadBB4GGComputation m => Players -> Int -> LPM Var Int () -> m ()
computeNextBestAssignments _ 0 _ =
  return ()
computeNextBestAssignments players num programM =
  do mResult <- liftIO $ runProgram program
     case mResult
       of Just result ->
            -- Sometimes it happens that the algorithm returns a solution that does not fulfill all constraints.
            -- This happens in particular regularly (always?) when there is no possible solution.
            -- Another case where I observed this was when one of the constraining linear functions had a very high coefficient (maybe the problem is caused by not approximating integers closely enough, so that the constraints are not stable under passing to the closest integer?).
            -- In some cases I tested, this did not occur when the linear program was exported to a file and then ran directly through glpk, or even reimported into glpk-hs and run that way.
            -- Maybe the reason for this is that different default options for the algorithm are used?
            if fulfillsConstraints program result
              then
                do assig <- liftIO $ extractAssignment result
                   addComputedAssignment (assig, assignmentScore players assig)
                   computeNextBestAssignments
                     players
                     (num - 1)
                     (nextProgram result)
              else
                liftIO $
                  hPutStrLn stderr
                    "Error: MILP returned solution that does not fulfill all\
                    \ constraints"
          Nothing ->
            return ()
  where
    program = execLPM programM
    nextProgram result =
      do programM
         -- cut off best solution
         let prefResult =
               M.filterWithKey
                 (\ var _ -> isRight var)
                 result
         leqTo' ("cut off previous best " ++ show num)
           ( linCombination
           $ M.foldMapWithKey
               (\ var value -> [(if value >= 1 then 1 else -1, var)])
               prefResult
           )
           (M.size (M.filter (>= 1) prefResult) - 1)

-- | Runs the given program and returns a potential 'Result'.
runProgram :: LP Var Int -> IO (Maybe Result)
runProgram program =
  do (_, mScoreResult) <- glpSolveVars (mipDefaults {tmLim = timeLimit}) program
     return $ fmap (fmap round . snd) mScoreResult

-- | Checks whether an assignment of variables to values fulfills the given constraints.
fulfillsConstraints :: LP Var Int -> Result -> Bool
fulfillsConstraints lp result = all fulfillsConstraint $ constraints lp
  where
    evalLinFunc vars = sum . M.mapWithKey (\ v c -> c * vars M.! v)
    inBounds Free _ = True
    inBounds (LBound l) x = l <= x
    inBounds (UBound u) x = x <= u
    inBounds (Equ e) x = x == e
    inBounds (Bound l u) x = l <= x && x <= u
    fulfillsConstraint (Constr _ f bounds) =
      inBounds bounds $ evalLinFunc result f

-- | A helper function that cuts a list into pieces of a given length (though the last one may be smaller).
divideList :: Int -> [a] -> [[a]]
divideList _ [] = []
divideList n _ | n < 1 = error "divideList called with non-positive parameter"
divideList n l = take n l : divideList n (drop n l)

-- | Returns one of the possible assignments corresponding to the given 'Result'.
extractAssignment :: Result -> IO Assignment
extractAssignment vars =
  do time <- getPOSIXTime
     return
       . M.fromList
       . map (hashAssignmentGame time &&& id)
       $ concatMap gameAssignments playedGames
  where
    playedGames =
      lefts . M.keys $ M.filter (>= 1) vars
    -- this returns one of the equivalent possibilties of dividing the players onto the copies of the game
    gameAssignments game =
      map (const (view gameIDGPV game) &&& S.fromList)
        . transpose
        . divideList (vars M.! Left game)
        $ playingPlayers game
    playingPlayers game =
      map (view playerIDPV)
        . filter ((== game) . view gameRangeVarPV)
        . rights
        . M.keys
        $ M.filter (>= 1) vars

-- | A helper type for determining the relevantly different ranges in the potential player numbers of a game.
-- It encompasses whether a player number is a minimum and/or maximum of a relevantly different player range.
type MinMax = (Sum Int, Sum Int)

-- | Is used to say that a player number occurrs as the minimum of a relevantly different player range.
minNum :: MinMax
minNum = (Sum 1, Sum 0)

-- | Is used to say that a player number occurrs as the maximum of a relevantly different player range.
maxNum :: MinMax
maxNum = (Sum 0, Sum 1)

-- | Given an ascending list of player numbers together with indication how often it occurrs as the the minimum and/or maximum of a range, computes the relevantly different ranges.
ranges :: Integral a => [(a, MinMax)] -> [(a, a)]
ranges = ranges' Nothing
  where
    -- The first parameter stores the most recent start of a range as well as the number of ranges one is currently in.
    ranges' :: Integral a => Maybe (a, Int) -> [(a, MinMax)] -> [(a, a)]
    ranges' _ [] = []
    ranges' mStart ((b, (Sum mi, Sum ma)) : r)
      | mi > 0 && ma > 0 =
          addRange (b - 1) $ (b, b) : ranges' (newMStart $ b + 1) r
      | mi > 0 =
          addRange (b - 1) $ ranges' (newMStart b) r
      | ma > 0 =
          addRange b $ ranges' (newMStart $ b + 1) r
      | otherwise =
          ranges' mStart r
      where
        addRange end =
          case mStart
            of Just (start, _) | start <= end -> ((start, end) :)
               _                              -> id
        newInRangesNum = maybe 0 snd mStart - ma + mi
        newMStart n
          | newInRangesNum > 0 = Just (n, newInRangesNum)
          | otherwise = Nothing

-- | Returns a linear program that computes the best currently possible assignment (or @Nothing@ if there are no preferences to be fulfilled).
linearProgram :: InstanceState -> Maybe (LPM Var Int ())
linearProgram state =
  -- test that there are actually preferences to be fulfilled (otherwise might lead to crashes)
  -- This should not happen (anymore?). I'm not sure if this still does something.
  condJust (not . null . rights . M.keys . allVars . execLPM) $
    do setDirection Max
       -- variables that represent the number of copies that are played of a certain game in a certain player number range
       gameCopiesVars <-
         M.traverseWithKey
           ( \ gID gd ->
               traverse
                 ( \ gameRange ->
                     do setVarKind (Left gameRange) IntVar
                        return gameRange
                 )
                 (gamePlayerRanges gID gd)
           )
           games
       -- whether a player plays a certain game in a certain player range
       prefVars <-
         sequence $
           do (pID, pData) <- M.toList players
              (gID, pref) <- M.toList $ pData ^. valueTS . prefPD
              gameCopiesVar <- fromMaybe [] $ gameCopiesVars M.!? gID
              -- by construction the player numbers in the range of 'gameCopiesVar' all have the same score
              let score =
                    gamePreferenceScore (gameCopiesVar ^. minPlayersGPV) pref
              guard $ score > 0
              return $
                do let var = PrefVar pID gameCopiesVar
                   setVarKind (Right var) BinVar
                   addObjective $ monomial score (Right var)
                   return var
       -- each player plays exactly one game
       for_ (M.keys players) $
         \ pID ->
           equalTo' ("player " ++ show pID ++ " plays exactly one game")
             ( linCombination
             . map ((1, ) . Right)
             . filter ((== pID) . view playerIDPV)
             $ prefVars
             )
             1
       -- at most the maximum number of copies of a game are played
       forWithKey_ games $
         \ gID gameData ->
           leqTo' ("game " ++ show gID ++ " is bounded by copies")
             ( linCombination
             . map ((1, ) . Left)
             $ gameCopiesVars M.! gID
             )
             ( gameData ^. maxCopiesGD
             - maybe 0 (copiesInAssignment gID)
                 (state ^? currentAssigIS . valueTS)
             )
       for_ gameCopiesVars
         . traverse_ $
             \ gameRange ->
               do let playerVars =
                        map Right $
                          filter
                            ((== gameRange) . view gameRangeVarPV)
                            prefVars
                      gameCopiesVar =
                        Left gameRange
                      playerNumFunc =
                        linCombination $ map (1, ) playerVars
                  -- a game is played by at least the minimum number of players to get the number of copies full, and at most the maximum number of players that fit in the number of copies
                  geq'
                    (  "game "
                    ++ show
                         ( gameRange ^. gameIDGPV
                         , gameRange ^. minPlayersGPV
                         , gameRange ^. maxPlayersGPV
                         )
                    ++ " min players"
                    )
                    playerNumFunc
                    (monomial (gameRange ^. minPlayersGPV) gameCopiesVar)
                  leq'
                    (  "game "
                    ++ show
                         ( gameRange ^. gameIDGPV
                         , gameRange ^. minPlayersGPV
                         , gameRange ^. maxPlayersGPV
                         )
                    ++ " max players"
                    )
                    playerNumFunc
                    (monomial (gameRange ^. maxPlayersGPV) gameCopiesVar)
  where
    players =
      waitingPlayers state
    games =
      M.filter (view isAvailableGD)
        . M.mapMaybe id
        . M.fromSet (\ gID -> state ^? gamesIS . gameDataG gID)
        $ preferredGameIDs players
    forWithKey_ m f =
      void $ M.traverseWithKey f m
    monomial c v =
      linCombination [(c, v)]
    condJust cond a
      | cond a = Just a
      | otherwise = Nothing
    gamePlayerRanges gID gd =
      do (minPlayers, maxPlayers) <-
           ranges
             . M.toAscList
             . M.fromListWith (<>)
             . concatMap
                 (prefPlayerRanges gd . (M.! gID) . view (valueTS . prefPD))
             . M.elems
             $ preferringPlayers players gID
         return $
           GameRangeVar
             { _gameIDGPV     = gID
             , _minPlayersGPV = minPlayers
             , _maxPlayersGPV = maxPlayers
             }
    prefPlayerRanges gd pref = rangesFrom (gd ^. minPlayersGD) Nothing
      where
        -- the second parameter is the score of the range one is currently in
        rangesFrom start Nothing
          | start > gd ^. maxPlayersGD = []
          | score start > 0 =
              (start, minNum) : rangesFrom (start + 1) (Just $ score start)
          | otherwise =
              rangesFrom (start + 1) Nothing
        rangesFrom start (Just s)
          | start > gd ^. maxPlayersGD =
              [(start - 1, maxNum)]
          | score start == s =
              rangesFrom (start + 1) $ Just s
          | score start > 0 =
              (start - 1, maxNum)
                : (start, minNum)
                : rangesFrom (start + 1) (Just $ score start)
          | otherwise =
            (start - 1, maxNum) : rangesFrom (start + 1) Nothing
        score n = gamePreferenceScore n pref
