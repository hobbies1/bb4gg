module BB4GG.Core.Constants where

import BB4GG.Types

import Data.Time (UTCTime)
import System.FilePath ((</>))

import qualified Data.Map as M


-- | Generates the path to the file containing the names of the instances from the path to the data directory.
instanceNamesDataPath :: FilePath -> FilePath
instanceNamesDataPath dataPath = dataPath </> "instances.dat"

-- | Generates the path to the file containing the data of the given instance.
instanceDataPath :: FilePath -> InstanceName -> FilePath
instanceDataPath dataPath name = dataPath </> "_" ++ name ++ ".dat"

-- | Generates an initial instance state with the given instance configuration
initialInstanceState :: UTCTime -> InstanceConfiguration -> InstanceState
initialInstanceState now ic =
  InstanceState
    { _configurationIS     = ic
    , _gamesIS             = M.empty
    , _playersIS           = M.empty
    , _computationStatusIS = RecomputeCS
    , _currentAssigIS      = stamp now M.empty
    }

-- | A rough upper bound for the number of users in an instance.
--
--  This limit is not enforced during player creation, but it is used as an upper bound for the maximum number of players and the number of copies for games.
userLimit :: Int
userLimit = 100

-- | The range of allowed preferences for a special game.
specialGamePreferenceRange :: SpecialGame -> (Score, Score)
specialGamePreferenceRange NotPlayingSG = (1, 10)

-- | Maximal possible preference score for a normal game.
maxPreference :: Score
maxPreference = 10

-- | Maximum length of the name of a game.
maxGameNameLength :: Int
maxGameNameLength = 30

-- | Maximum length of a game comment.
maxGameCommentLength :: Int
maxGameCommentLength = 20

-- | Maximum length of the name of a player.
maxPlayerNameLength :: Int
maxPlayerNameLength = 20

-- | Maximum length of the name of an instance.
maxInstanceNameLength :: Int
maxInstanceNameLength = 30
