{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE UndecidableSuperClasses #-}
{-# LANGUAGE QuantifiedConstraints #-}

module BB4GG.Core.Monad.BB4GG
 ( module BB4GG.BGG.Request
 , MonadBB4GG
 , MonadBB4GG' (..)
 , MonadUnlockable'
 , UnlockT'
 , runUnlocked'
 ) where

import BB4GG.Config
import BB4GG.Types

import BB4GG.BGG.Request ( MonadBGG (..)
                         , requestBGGSearch
                         , retrieveBGGThing
                         , retrieveBGGThings
                         )
import BB4GG.Core.Monad.Trans.BB4GGInstance (MonadBB4GGInstance, LockIMax)
import BB4GG.Util.Monad.HeistState (MonadHeistState)
import BB4GG.Util.Monad.Unlockable (MonadUnlockable', UnlockT', runUnlocked')

import Control.Monad.Morph (hoist)
import Control.Monad.Trans (MonadIO, MonadTrans, lift)

import qualified Data.Set as S


-- | A class that encapsulates the necessary functions to interact with the main application.
-- Has a single lock which needs to be acquired to add or remove instances.
-- When this functionality is required the constraint @'MonadBB4GG' c@ needs to be used instead (it adds the ability to actually acquire the lock).
-- The constraint @c@ is used to pass functionality from the base monad through to the instances.
class (MonadHeistState m, MonadBGG m, c m) => MonadBB4GG' c m | m -> c where
  -- | The configuration BB4GG is running with.
  getConfig :: m Bb4ggConfig
  -- | The names of the currently existing instances.
  getInstanceNames :: m (S.Set InstanceName)
  -- | Adding a new instance.
  -- The lock needs to be acquired beforehand.
  newInstance :: InstanceName -> UnlockT' m Bool
  -- | Deleting an instance.
  -- The lock needs to be acquired beforehand.
  deleteInstance :: InstanceName -> UnlockT' m Bool
  -- Running a 'MonadBB4GGInstance' action for the instance with the provided name.
  runInstance
    :: (forall n . (MonadBB4GGInstance LockIMax c n, MonadBGG n) => n a)
    -> InstanceName
    -> m (Maybe a)

-- | This constraint is @'MonadBB4GG'' c@ plus 'MonadUnlockable' functionality.
type MonadBB4GG c m = (MonadBB4GG' c m, MonadUnlockable' (MonadBB4GG' c) m)

instance {-# OVERLAPPABLE #-}
  ( MonadBB4GG' c m
  , MonadTrans t
  , MonadIO (t m)
  , c (t m)
  ) => MonadBB4GG' c (t m)
  where
  getConfig = lift getConfig
  getInstanceNames = lift getInstanceNames
  newInstance = hoist lift . newInstance
  deleteInstance = hoist lift . deleteInstance
  runInstance instM = lift . runInstance instM
