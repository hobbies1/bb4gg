{-# LANGUAGE TemplateHaskell #-}

module BB4GG.Types.Assignment where

import BB4GG.Types.Game
import BB4GG.Types.Player

import Data.Aeson.TH (defaultOptions, deriveJSON, fieldLabelModifier)
import Control.Lens (makeLenses)

import qualified Data.Map.Strict as M
import qualified Data.Set as S


-- | The identifier of an instance of a game in an assignment.
-- Will be a hashed value.
type AssignmentGameID = Int

-- | Contains the data of an assignment of players to games.
type Assignment = M.Map AssignmentGameID (GameID, S.Set PlayerID)

-- | The identifier of an assignment.
-- Will be a hashed value.
type AssignmentID = Int

-- | The overall score of an assignment.
-- The order of the constructors is relevant because (the reverse of) the derived @Ord@ instance is used while ordering assignments.
data AssignmentScore = AssignmentScore
  { -- | The sum of the scores the players each gave to the game they are supposed to play in the assignment in question.
    _sumAS     :: Score
    -- | The minimum of the scores the players each gave to the game they are supposed to play in the assignment in question.
  , _minimumAS :: Score
  } deriving (Eq, Ord, Show)

makeLenses ''AssignmentScore
$(deriveJSON defaultOptions{fieldLabelModifier = drop 1} ''AssignmentScore)

-- | A collection of assignments with their score, indexed by id.
type Assignments = M.Map AssignmentID (Assignment, AssignmentScore);
