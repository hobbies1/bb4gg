{-# LANGUAGE TemplateHaskell #-}

module BB4GG.Types.Game where

import BB4GG.BGG.Types

import Data.Aeson (FromJSONKey, ToJSONKey)
import Data.Aeson.TH (defaultOptions, deriveJSON, fieldLabelModifier)
import Control.Lens (Fold, (^.), folding, makeLenses)
import Data.Function (on)
import Data.Hashable (Hashable)
import Data.SafeCopy (base, deriveSafeCopy)
import GHC.Generics (Generic)

import qualified Data.Map.Strict as M


-- | Contains the data associated to a game.
data GameData = GameData
  { _nameGD                  :: String -- ^ Name of the game.
  , _commentGD               :: String -- ^ A comment about the game.
  , _bggIDGD                 :: Maybe BGGID -- ^ The BGG ID of this game (if it exists).
  , _minPlayersGD            :: Int    -- ^ Minimum number of players for the game.
  , _maxPlayersGD            :: Int    -- ^ Maximum number of players for the game.
  , _recommendedMinPlayersGD :: Int    -- ^ Default minimum number of players for preferences.
  , _recommendedMaxPlayersGD :: Int    -- ^ Default maximum number of players for preferences.
  , _maxCopiesGD             :: Int    -- ^ Number of copies available when the game is.
  , _isAvailableGD           :: Bool   -- ^ Whether the game is available at all.
  } deriving (Eq, Show, Generic)

makeLenses ''GameData

deriveSafeCopy 3 'base ''GameData
$(deriveJSON defaultOptions{fieldLabelModifier = drop 1} ''GameData)

-- | Compares games first by their names, then the minimum number of players and then the maximum number of players.
instance Ord GameData where
  compare = compare `on` info
    where info g = (g ^. nameGD, g ^. minPlayersGD, g ^. maxPlayersGD)

-- | A type for special (hardcoded) games.
data SpecialGame = NotPlayingSG deriving (Eq, Ord, Show, Generic)

instance Hashable SpecialGame where
deriveSafeCopy 1 'base ''SpecialGame
$(deriveJSON defaultOptions ''SpecialGame)

-- | The hardcoded games
specialGameData :: SpecialGame -> GameData
specialGameData NotPlayingSG =
  GameData
    { _nameGD                   = "Not playing"
    , _commentGD                = ""
    , _bggIDGD                  = Nothing
    , _minPlayersGD             = 1
    , _maxPlayersGD             = 1000
    , _recommendedMinPlayersGD  = 1
    , _recommendedMaxPlayersGD  = 1000
    , _maxCopiesGD              = 1
    , _isAvailableGD            = True
    }

-- | Identifier for a non-special game.
type NormalGameID = Word

-- | Identifier of either a special game or a normal one.
data GameID =
    SpecialGID SpecialGame
  | NormalGID NormalGameID
  deriving (Eq, Ord, Show, Generic)

instance Hashable GameID where
deriveSafeCopy 1 'base ''GameID
instance FromJSONKey GameID where
instance ToJSONKey GameID where
$(deriveJSON defaultOptions ''GameID)

-- | Representation of multiple games.
type Games = M.Map NormalGameID GameData

-- | Accesses the 'GameData' belonging to a certain 'GameID' (if it exists).
_gameDataG :: GameID -> Games -> Maybe GameData
_gameDataG (SpecialGID special) _ = Just $ specialGameData special
_gameDataG (NormalGID normal) gs = gs M.!? normal

-- | A 'Fold' for accessing the 'GameData' belonging to a certain 'GameID' (if it exists).
gameDataG :: GameID -> Fold Games GameData
gameDataG gID = folding $ _gameDataG gID
