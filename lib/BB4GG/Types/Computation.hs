{-# LANGUAGE TemplateHaskell #-}

module BB4GG.Types.Computation where

import BB4GG.Types.Assignment
import BB4GG.Types.Player

import Data.Aeson.TH (defaultOptions, deriveJSON)
import Control.Concurrent (ThreadId)
import Control.Concurrent.MVar (MVar)
import Control.Lens (Traversal', makeLenses)
import Data.Time (UTCTime (..))

import qualified Data.Set as S


-- | The computation of the currently best assignments.
-- Contains the @ThreadId@ of the currently computing thread if there is one, as well as the already computed assignments.
data Computation = Computation
  { -- | 'Just' the computing thread, 'Nothing' if it is already done
    _threadIdC            :: Maybe ThreadId
    -- | The already computed 'Assignment's
  , _computedAssignmentsC :: Assignments
  }

makeLenses ''Computation

-- | Enum type for errors occurring when computing assignments.
data ComputationError =
    NoActivePlayersCE
  | PlayersNotYetReadyCE (S.Set PlayerID)
  | PlayersWithoutPreferenceCE (S.Set PlayerID)

$(deriveJSON defaultOptions ''ComputationError)

-- | Status of the computation.
-- The type parameter 'c' is the type containing information about the computation.
data ComputationStatus' c =
    NotLockedCS
      ( Either
          ComputationError
          c
      )                  -- ^ Results are not locked, and either it is impossible to compute assignments or a computation is possibly running.
  | RecomputeCS          -- | Results should be (re)computed the next time the results page is accessed.
  | UnlockAtCS
      UTCTime
      c -- ^ A computation is possibly running and the results are locked until the given time.
  | RecomputeAtCS
      UTCTime
      c -- ^ A computation is possibly running, the results are locked until the given time, and the results should be recomputed when the lock has expired.
  deriving (Functor, Foldable, Traversable)

$(deriveJSON defaultOptions ''ComputationStatus')

-- | Status of the computation with threaded access. 
type ComputationStatus = ComputationStatus' (MVar Computation)

-- | A 'Traversal'' for accessing the unlock time potentially contained in a 'ComputationStatus'.
unlockTimeCS :: Traversal' (ComputationStatus' c) UTCTime
unlockTimeCS f (UnlockAtCS t c)    = UnlockAtCS <$> f t <*> pure c
unlockTimeCS f (RecomputeAtCS t c) = RecomputeAtCS <$> f t <*> pure c
unlockTimeCS _ x                   = pure x

-- | A 'Traversal'' for accessing the computation potentially contained in a 'ComputationStatus'.
compCS :: Traversal' (ComputationStatus' c) c
compCS = traverse
