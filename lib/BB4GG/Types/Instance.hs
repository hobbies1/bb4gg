{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module BB4GG.Types.Instance
  ( module BB4GG.Types.Instance
  , module BB4GG.Web.Websockets.Server
  , module BB4GG.Util.Timestamped
  ) where

import BB4GG.Types.Game
import BB4GG.Types.Player
import BB4GG.Types.Assignment
import BB4GG.Types.Computation
import BB4GG.Util.Timestamped
import BB4GG.Web.Websockets.Server hiding (initWebsocketsServer)

import BB4GG.Config (NewInstanceConfig (..))

import Data.Aeson.TH (defaultOptions, deriveJSON, fieldLabelModifier)
import Control.Concurrent (ThreadId)
import Control.Concurrent.STM (TVar, TMVar)
import Control.Lens (makeLenses)
import Data.Ix (Ix)
import Data.SafeCopy (base, deriveSafeCopy)
import Data.Time (NominalDiffTime)

import qualified Data.Array as A
import qualified Data.Map.Strict as M


-- | Configuration for an instance of the application.
data InstanceConfiguration = InstanceConfiguration
  { -- | Whether the instance should appear on the public list of instances.
    _listedIC               :: Bool
    -- | The maximum number of best 'Assignment's to compute.
  , _assignmentsToComputeIC :: Int
    -- | Whether all active players have to mark themselves as "ready" before possible assignments are displayed and if that's the case, for how long the results should be locked once everyone is ready.
  , _requireReadyIC         :: Maybe NominalDiffTime
    -- | The time (in hours) to wait before automatically deactivating a player or assignment (if `Nothing`, never do it).
  , _autoDeactivationHoursIC :: Maybe Int
  } deriving (Eq, Show)

makeLenses ''InstanceConfiguration

deriveSafeCopy 2 'base ''InstanceConfiguration
$(deriveJSON
    defaultOptions{fieldLabelModifier = drop 1}
    ''InstanceConfiguration)

toInstanceConfiguration :: NewInstanceConfig -> InstanceConfiguration
toInstanceConfiguration (NewInstanceConfig l atc rr adh) =
  InstanceConfiguration l atc rr adh

-- | The state of a single instance of the application.
data InstanceState = InstanceState
  { -- | Configuration for the current instance.
    _configurationIS     :: InstanceConfiguration
    -- | Games in question.
  , _gamesIS             :: Games
    -- | Present players.
  , _playersIS           :: Players
    -- | The status of the computation of results.
  , _computationStatusIS :: ComputationStatus
    -- | 'Just' the chosen assignment (and the time it was last changed) if there is one, 'Nothing' otherwise.
  , _currentAssigIS      :: Timestamped Assignment
  }

makeLenses ''InstanceState

-- | The part of an 'InstanceState' that will be saved on disk (everything except for the running computation).
data SavableInstanceState = SavableInstanceState
  { _configurationSIS :: InstanceConfiguration
  , _gamesSIS         :: Games
  , _playersSIS       :: Players
  , _currentAssigSIS  :: Timestamped Assignment
  } deriving (Eq, Show)

makeLenses ''SavableInstanceState

deriveSafeCopy 2 'base ''SavableInstanceState
$(deriveJSON defaultOptions{fieldLabelModifier = drop 1} ''SavableInstanceState)

-- | Extracts the part that will be saved on disk (everything except the computed assignments).
toSavableInstanceState :: InstanceState -> SavableInstanceState
toSavableInstanceState InstanceState {..} =
  SavableInstanceState
    { _configurationSIS        = _configurationIS
    , _gamesSIS                = _gamesIS
    , _playersSIS              = _playersIS
    , _currentAssigSIS         = _currentAssigIS
    }

-- | Reconstructs the instance state from saved information, initialising no current computation.
fromSavableInstanceState :: SavableInstanceState -> InstanceState
fromSavableInstanceState SavableInstanceState {..} =
  InstanceState
    { _configurationIS        = _configurationSIS
    , _gamesIS                = _gamesSIS
    , _playersIS              = _playersSIS
    , _computationStatusIS    = RecomputeCS
    , _currentAssigIS         = _currentAssigSIS
    }

type InstanceName = String

-- | A value level representation of the diffeent lock levels of an instance.
-- This really belongs into 'BB4GG.Core.Monad.BB4GGInstance', but can't be put there due to module import constraints.
data LocksI = LockIBr | LockISv | LockISt | LockIBg
  deriving (Bounded, Enum, Eq, Ix, Ord)

-- | A single instance of the application.
data Instance = Instance
  { -- | Where the instance data is saved.
     _dataPathI :: FilePath
    -- | The state of the instance.
  ,  _stateI :: TVar InstanceState
    -- | The various locks an instance uses.
  , _locksI :: A.Array LocksI (TMVar ThreadId)
    -- | The websockets server.
  , _wsServerI :: WSServer
  }

makeLenses ''Instance

-- | Multiple instances.
type Instances = M.Map InstanceName Instance
