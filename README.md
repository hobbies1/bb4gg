# Blackboard for Gaming Groups

Blackboard for Gaming Groups (BB4GG) is a web application that helps gaming groups decide what to play.

## Building

BB4GG is intended to be built using [Stack][stack].
Once Stack is installed, you can run `stack build` in the repository to build BB4GG, after which it can be run with `stack exec bb4gg`.

## Configuration

The parameters in the table below can be set using [Conferer][conferer]'s "[Basic Sources][conferer-basics]".

For example, you can set the port BB4GG is served on to `8080` by running it with the option `--snap.port=8080`.
(Note that if you are using `stack exec`, you should escape the options for BB4GG by executing `stack exec bb4gg -- <OPTIONS>`.)

| Group      | Parameter                       | Default                           | Comments                                                                                           |
|------------|---------------------------------|-----------------------------------|----------------------------------------------------------------------------------------------------|
| `snap`     | see [Snap sources][snap-config] | see [Snap sources][snap-default] | configuration for the [Snap web server][snap-server]                                               |
| `dirs`     | `data`                          | `data`                           | path for data files                                                                                |
| `dirs`     | `bb4gg`                         | `.`                              | path to BB4GG files (containing directories called `static` and `templates` as in the repository)  |
| `instance` | `listed`                        | `True`                           | whether newly created instances should be shown in the public list of instances                    |
| `instance` | `assignmentsToCompute`          | `10`                             | the number of best assignments to compute for newly created instances                              |
| `instance` | `requireReady`                  | `Nothing`                        | whether all players need to be marked as ready before results are shown in newly created instances |
| `instance` | `autoDeactivationHours`         | `12`                             | time (in hours) after which a player is automatically deactivated                                  |
| `bgg`      | `cacheMax`                      | `Nothing`                        | maximum number of BGG items to cache                                                               |
| `bgg`      | `cacheDays`                     | `7`                              | time (in days) to cache BGG items                                                                 |
| `bgg`      | `maxThingsToRequest`            | `500`                            | the maximum number of things to request from the BGG API at once
| `bgg`      | `minDelay`                      | `0.1s`                           | the minimum delay (in seconds) between requests to the BGG API
| `bgg`      | `delayIncreaseOn429`            | `5`                              | by which factor the delay between requests should be increased on a 429 (Too Many Requests) response
| `bgg`      | `delayDecreasePerMin`           | `0.5`                            | by what factor the delay between requests should decrease per minute (down to 'minDelay')

## Generating Sample Instances for Testing

If the first command line argument passed to the BB4GG executable is `add-sample-instance`, a sample instance with some games and players will be created.
After that, the server will be started as usual -- in particular, it can be configured with further command line arguments.

[stack]: https://haskellstack.org
[conferer]: https://conferer.ludat.io/
[conferer-basics]: https://conferer.ludat.io/docs/sources/basics
[snap-config]: https://hackage.haskell.org/package/snap-server/docs/src/Snap.Internal.Http.Server.Config.html#Config
[snap-default]: https://hackage.haskell.org/package/snap-server/docs/src/Snap.Internal.Http.Server.Config.html#defaultConfig
[snap-server]: https://hackage.haskell.org/package/snap-server
