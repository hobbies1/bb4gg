Haskell Style Guide
===================

These are the style conventions we use in this project.
They should hopefully cover most situations that arise normally.
Not covered are type classes and their instances, non-language pragmas and infix-statements (and possibly things we forgot).


# Some General Principles which are Partially Repeated Later

The maximum line length is 80 characters, except for comments.

## Whitespace

* Do not use tabs. Indention length is usually two spaces.
* Do not put trailing whitespaces.
* Put one blank line between two top-level defintions and at the end of the file.
* Put a space before and after a binary operator.
* Do not put a space after an opening bracket if the closing bracket is in the same line.
* If an expression with brackets spans over multiple lines, put a space after the opening bracket (and align everything in the expression accordingly), except if it is a section of an infix operator with the operator in the front.

## Aligning Things

* Align `=`, `::` etc. as long as there is no line break in the expressions which come after them.
* If there is a closing bracket that is not in the same line as the opening bracket, align it to the opening bracket.

Examples:
```haskell
map f []       = []
map f (x : xs) = f x : xs

foo (Just something) =
  do this
     return that
foo Nothing =
  bla

functionOne
  ( functionTwo
      veryVeryVerylongParameter
  )
  thisOrThat
  (functionThree something)
```

## Comma/Operator-Separated Things

* Always use a space after a comma or an operator, except if it is at the end of a line.
* If a list, tuple, chain of operators etc. fits into a line, use only one line (with the exception of type declarations).
* Otherwise:
  - If it is inside of brackets (e.g. a list), put a newline before each comma/operator and align the commas/operators to brackets.
  - If it is not in brackets, put a newline before each operator and indent them two spaces.
  - If you have two expressions joined by a single `$` instead you may put the newline after the `$` and indent the second expression two spaces.
  - If all arguments fit in one line each or all operators have three characters or less, align the arguments.
  Otherwise put newlines after the operators longer than three characters and indent these arguments two spaces from the operator.

Examples:
```haskell
(fst, snd)

number + anotherNumber + thirdNumber

thisIsAVeryVeryVeryVeryLongNumber
  + thisISAnotherVeryVeryLongNumber

someFunctionWhichIsAppliedToAnotherFunction $
  firstFunction . secondFunction

[ "Hello, world!"
, "Hallo, Welt!"
, "Ciao mondo!"
, "Olá mundo!"
, "Merhaba dünya!"
]

someString
  ++        "some other string"
  `mappend` anotherString

(         someString
++        "some other string"
`mappend` anotherString
)

someString
  ++ "some string which is very long"
  `mappend`
    someFunctionWhichIsLong argument
  `app`
    "another string"
  ++ lastString

( someString
++ "some string which is very long"
`mappend`
  someFunctionWhichIsLong argument
`app`
  "another string"
++ lastString
)

( 43709532698746329845324324654832458632583264832
+ someOtherNumberWeHaveCalculatedBefore
+
)

pureFunction
  <$> thisIsSomeFunction
        someArgumentWhichIsQuiteLong
        someOtherArgument
  <*> secondApplicativeArgument
  <*> pure something

(   pureFunction
<$> thisIsSomeFunction
      someArgumentWhichIsQuiteLong
      someOtherArgument
<*> secondApplicativeArgument
<*> pure something
)
```

## Naming Conventions

* Types: `UpperCamelCase`
* Constructors: `UpperCamelCase` (equal to the upper camel case type name) when there is only one constructor, otherwise `UpperCamelCaseUCS` (upper camel case plus the abbreviation of the upper camel case type name as a suffix)
* Fields: `lowerCamelCaseUCS` (lower camel case plus the abbreviation of the upper camel case constructor name as a suffix)
* Top level functions: `lowerCamelCase`
* Local variables:
  - For inhabitants of type variables like `a` you may use generic names like `x`.
  This also descends to types constructed from such types, e.g. an inhabitant of `[a]` may be called `l`.
  - In general, name a variable of type `CamelCase` `camelCase`.
  - `cc` (lower case abbreviation) is acceptable if the type is clear from the context (e.g. for arguments while defining top-level functions or in lambda expressions).
  In that case, if the long name already contains an abbreviation, keep it upper case, as in `ccID` for an inhabitant of `camelCaseID`.
  - Use `mCamelCase` or `mCc` for an inhabitant of `Maybe CamelCase`.
  - Use `camelCases` (in which you should also use the correct plurals) or `ccs` for an inhabitant of `[CamelCase]` or any other container type containing `CamelCase`.
  - Primes may be added to produce multiple variables of the same type: `cc`, `cc'`, `cc''`...
  - Feel free to use more descriptive names than these if it helps to understand the code.


# Front Matter

The following is the general structure of a file:
1. Language pragmas.
2. A blank line.
3. The module declaration.
4. A blank line.
5. The imports.
6. Two blank lines.
7. Everything else.

## Language Pragmas

* Have all language pragmas that are used in a file in that file.
* Put one language pragma per line and sort them alphabetically.

Example:
```haskell
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
```

## Module Declaration

* Always explicitly list the types then the functions that should be exported by the module and sort each of those alphabetically in itself.
* Have a single module declaration line if it is possible.
Otherwise use the usual configuration for comma-separated things (one item per line, commas aligned to parentheses) with no newline before the opening parenthesis.
* The `where` comes after the closing parenthesis, separated by a single space.

Examples:
```haskell
module BB4GG.Types ( Assignment
                   , GameData (..)
                   , GameID
                   , Games
                   , PlayerData (..)
                   ) where

module BB4GG.Web.Page.Instance.Results (resultsPage) where
```

## Imports

There should be up to four groups of imports in the following order:
1. Imports from core internal modules.
2. Unqualified imports from internal modules.
3. Qualified imports from internal modules.
4. Unqualified imports from external libraries.
5. Qualified imports from external libraries.

The general principles are:
* Order each group alphabetically.
* For qualified imports, use abbreviations of camel case names.
* For unqualified imports from non-core modules, list the imported symbols
explicitly.
  - List classes and types first, and then functions.
  - Order each of these alphabetically within itself (where special characters come before all letters).
  - If you don't use any fields/functions of a type, just import its name.
  Otherwise use `SomeType (..)`, i.e. import all the fields/functions.
  - If possible, keep everything related to an import in one line.
  Otherwise, use the usual configuration for comma-separated things (one item per line, commas aligned to parentheses) with no newline before the opening parenthesis.

Example:
```haskell
import BB4GG.Types

import BB4GG.Web.Page.WebUtilities (renderInstancePage)

import qualified BB4GG.Tests.GameData as GD

import Snap.Core (Snap, getRequest, redirect, rqParams)
import Text.Blaze ((!), toValue)
import Text.Digestive.Blaze.Html5 ( form
                                  , inputCheckbox
                                  , inputSubmit
                                  , inputText
                                  , label
                                  )

import qualified Data.ByteString.Char8 as BS
import qualified Data.Map.Strict as M
```


# Type Declarations

* Use upper camel case for type names.
* If there is only one constructor its name should coincide with the name of the type.
* For sum types:
  - For each constructor use upper camel case and add the abbreviation of the
type name as a suffix.
  - Use one line if all constructors fit into a line.
  Otherwise put one constructor per line with newlines after `=` and before each `|`, indenting `|` two spaces and aligning the constructor names.
* If there are named fields:
  - For each field name use lower camel case and add the abbreviation of the constructor name as a suffix.
  - Put a line break after the constructor name.
  - Indent the records two spaces from the name of the constructor.
  - Use one line for each field name in the usual configuration for comma-separated things.
  - Align the `::` according to usual aligning rules.

Examples:
```haskell
data MyType = MyType
  { firstFieldMT  :: Int
  , secondFieldMT :: Int
  , thirdFieldMT  :: Int
  , fourthFieldMT :: Double
  }

data Color = BlueC | YellowC | GreenC

data AlignedConstructors =
    ThisIsAnAlignedConstructorAC Int
  | ItReallyIsAC String
  | InTheVeryDeedAC

data LongConstructors =
    ThisIsAVeryLongConstructorLC
  | NotSoLongButWithFieldsLC
      { firstFieldNSLBWFLC  :: Int
      , secondFieldNSLBWFLC :: String
      }
  | ThisIsAlsoQuiteLongLC
```


# Type Signatures

* Always have type signatures for top-level defintions.
* Type signatures belong directly before the definition and are not separated by a blank line.
* If there is only one type class constraint, do not put parentheses around it.
* If the type-signature fits in one line and no argument has a comment, use only one line.
* Otherwise:
  - Have a newline after `::`, before `=>` and before each `->`.
  - Align the `=>` and `->`. Also align the types.
  - Indent everything two spaces.
  - Use the general guidelines for comma-separated things for the type class constraints.

Examples:
```haskell
modifyPlayerForm ::
  Monad m
  => Maybe PlayerData
  -> (String -> Result String String)
  -> Games
  -> Form String m PlayerData

preferenceForm ::
     Maybe Preference
  -> GameID
  -> GameData
  -> Form String m Preference

sortMap :: (Ord k, Ord a) => ((k, a) -> b) -> M.Map k a -> [b]

function ::
  ( LongAndVeryComplicatedAndAbstractTypeClass c,
  , AnotherTypeClass d
  )
  => [c]
  -> c
  -> d c
```


# Function Definitions

* For top-level functions you should always add a type signature.
You can put additional type signatures if it helps to understand the function.
* Put a newline before each `|` and indent them two spaces.
* If all pattern matching cases fit in one line each (this implies that there are no guards), use one line each and align the arguments and the `=`.
* If at least one case does not fit in one line (e.g. because there is a guard), have a newline after each case (after the `=` if the case does not have guards, otherwise after the last argument) and indent two spaces.
* If all definitions after the guards of a single pattern matching case fit in one line, use one line each and align the `=`. Otherwise have a newline after each of the `=` and indent two spaces from the `|`.

```haskell
function (Just blub) _           _    = f blub
function Nothing     (Just blob) _    = g blob
function Nothing     Nothing     blab = h blab

function (Just a) b c =
  f a b c
function Nothing (Just b') c
  | test b'   = g c
  | otherwise = h c
function Nothing Nothing c =
  | test c =
    veryLongFunctionNameOfComplicatedFunction c
  | anotherTest c =
    someConstant
  | otherwise =
    g c
```


# Function application and composition

* If the application of a function fits in one line, use only one line.
* Otherwise have each argument on its own line and indent them two spaces.
Use your judgement when deciding what qualifies as a function and what as an argument (e.g. it is not clear if `flip div` is semantically a function or a function and an argument).
* Write application of composed functions like `f . g . h $ x`, i.e. use `.` to compose functions and then apply the compositions with `$`.
Bear in mind the rules for operator-separated things.

Examples:
```haskell
function a b

function
  veryVeryVeryLongArgument
  anotherArgument

function
  . ( veryVeryLongFunction
        argument
        something
        parameter
    )
  . anotherFunction
  $ argument
```


# Keywords

In general, if the expression after a keyword with more than three characters (i.e. not `if`, `do`, `of`, `let` or `in`) does not fit into a single line, put a newline after the keyword.

## `do`

* Do not have a newline after `do`.
* Align everything to one space after the `do`.
* Avoid brackets spanning multiple lines and multiline strings on the level of the `do` block if possible.

Example:
```haskell
do foo
   bar
```

## `where`

* Indent `where` two spaces.
* If it's only one one-line definition, have it in the same line as `where`, otherwise have a newline after `where`.
* If you have such a newline, indent the code two spaces.
* If all assignments are one-liners:
  - Have all assignments in the same line as `=`.
  - Align the `=`.
* If at least one assignment does not fit in one line:
  - Have a newline after each `=`.
  - Indent a further two spaces.
  - Do not align the `=`.

Examples:
```haskell
where foo = bar

where
  foo =
    do bar
       rest

where
  foo      = bar
  spamSpam = eggs

where
  foo =
    do bar
       rest
  spamSpam =
    eggs
```

## `let`

* Do not have a newline after `let`.
* Indent all definitions to one space after the `let`.
* If all assignments are one-liners:
  - Have all assignments in the same line as `=`.
  - Align the `=`.
* If at least one assignment does not fit in one line:
  - Have a newline after each `=`.
  - Indent a further two spaces.
  - Do not align the `=`.

Examples:
```haskell
let foo = bar

let foo =
      do bar
         rest

let foo      = bar
    spamSpam = eggs

let foo =
      do bar
         rest
    spamSpam =
      eggs
```

## `let in`

* Everything from `let` also applies here.
* Align the `l` from `let` and the `i` from `in`.
* Do not have a newline after `in`  and indent everything to one space after the `in`.

Example:
```haskell
let bla = 1
in [ bla
   , thisIsSomeFunctionWhichIsLong bla
   , bla
   ]
```

## `case of`

* Use the pattern matching of function definitions instead of `case of` when applicable.
* Have `of` in a new line and indented two spaces (i.e. align the `e` of `case` and the `f` of `of`).
* Do not have a newline after `of`.
* If the expression after `case` does not fit into one line, have a newline after `case` and indent two spaces.
* Align the cases to one space after `of`.
* Have a space before `->`.
* If all cases are one-liners, have them in the same line as `->` and align the `->`.
Otherwise have newlines after each `->` and do not align the `->`.
* If you have such newlines, align the code two spaces from the beginning of the cases.

Examples:
```haskell
case maybeSomething
  of (Just something) -> spam
     Nothing          -> eggs

case
  thisIsSomeLongFunctionThatMaybeReturnsSomething
    someArgument
    someOtherArgument
  of (Just something) ->
       do spam
          ham
     Nothing ->
       eggs
```

## `if then else`

* If the whole `if then else` block fits into one line, use only one line.
Otherwise use a newline before `then` and `else`, indenting them two spaces.
* Do not have a newline after `if`.
* If the `then` and the `else` block fit into one line each, use one line each.
Otherwise insert a newline afterboth `then` and `else` and indent two spaces.

Examples:
```haskell
if worldHasEnded then bla else blub

if worldHasEnded
  then
    ohNoWeNeedToPanicRightNowFunction
      apocalypse
      zombies
  else
    thisIsFine

if longFunctionName
     firstVeryVeryLongArgumentName
     secondArgumentName
  then longishValueName
  else anotherValueName
```

## Lambdas

* Always put a space after `\`.
* Use one line if it fits. Otherwise have a newline after `->` and indent two spaces from the `\`.

Examples:
```haskell
\ x -> x

\ x y ->
  (veryLongFunctionName x y, anotherVeryLongFunctionName x y)
```


# Multiline Strings

* Use `\` for multiline strings.
* Align the `\` at the beginning of a line with the opening `"`.
* End a multiline string with `\"` in its own line.

Example:
```haskell
string =
  "This is a very long string \
  \that spans multiple lines. \
  \See? It is still going on.\
  \"
```


# Documentation

* Document every top-level function.
* Use Haddock syntax.
* Write properly capitalized sentences with punctuation.
* Have one sentence per line. Remember that there is no charcter line limit for comments.
* Top-level definitions and declarations:
  - Have the comment be first, i.e. use `|` to invoke Haddock.
  - Use line comments, i.e. `--`.
  - Have a space after each `--` and after `|`.
* Function arguments and record fields:
  - Document them if it is not clear what they are.
  - Have the comment in the same line, after the thing you are commenting, i.e. use `^` to invoke Haddock.
  - Align the `--`.
  - Have a space before `--` and before and after `^`.

Examples:
```haskell
-- | This is a great documentation.
function :: a -> b
function = undefined

-- | This is an even greater documentation.
-- It has two sentences.
function ::
  TypeClass a
  => a -- ^ This is the first argument.
  -> b -- ^ This is the result.

-- | This is a very very very very very very very very very very long documentation.
function ::
     a -- ^ There is really really really quite a lot to say about this very complicated argument.
  -> b -- ^ Not so for this one.
  -> c -- ^ The result.
```
