{-# LANGUAGE PartialTypeSignatures #-}
{-# OPTIONS_GHC -Wno-partial-type-signatures #-}

module Main (main) where

import BB4GG.Core

import BB4GG.Core.AutoDeactivate (autoDeactivate)
import BB4GG.Web (site)
import BB4GG.Tests.SampleInstance (addSampleInstance)
import BB4GG.Util (periodicAction)

import Conferer (mkConfig, fetch)
import Control.Lens ((^.))
import Control.Monad (void)
import Control.Monad.Extra (whenM)
import Control.Monad.IO.Class (MonadIO)
import Data.Foldable (traverse_)
import Data.Maybe (catMaybes)
import Data.SafeCopy (safeGet)
import Data.Serialize (runGet)
import Data.Set.Lens (setOf)
import Snap.Core (MonadSnap)
import Snap.Http.Server ( ConfigLog (..)
                        , getAccessLog
                        , getErrorLog
                        , simpleHttpServe
                        )
import System.Directory (createDirectoryIfMissing)
import System.Environment (getArgs)
import System.FilePath ((</>), takeDirectory)
import System.IO (hPutStrLn, stderr)
import System.IO.Error (catchIOError, isDoesNotExistError)

import qualified Data.ByteString.Char8 as BS
import qualified Data.Map.Strict as M


-- | Extracts the file path from a 'ConfigLog' if it contains one.
fileLogPath :: ConfigLog -> Maybe FilePath
fileLogPath (ConfigFileLog p) = Just p
fileLogPath _                 = Nothing

-- | Creates the directories that show up in the configuration but do not exist yet.
createMissingDirectories :: Bb4ggConfig -> IO ()
createMissingDirectories config =
  do createDirectoryIfMissing True (config ^. dirsBC . dataDC)
     let createLogDirectoryIfMissing getter =
           traverse_
             (createDirectoryIfMissing True . takeDirectory)
             (fileLogPath =<< getter (config ^. snapBC))
     createLogDirectoryIfMissing getAccessLog
     createLogDirectoryIfMissing getErrorLog

-- | This is an error handler that, if an error indicating the non-existence of a file is raised, creates a new instance name file that is empty.
instanceNamesReadHandlerWith :: FilePath -> IOError -> IO String
instanceNamesReadHandlerWith p e
  | isDoesNotExistError e =
    do writeFile (instanceNamesDataPath p) ""
       return ""
  | otherwise =
    ioError e

-- | This is an error handler that, if an error indicating the non-existence of a file is raised, instead returns the empty string.
instanceReadHandler :: IOError -> IO BS.ByteString
instanceReadHandler e
  | isDoesNotExistError e = return ""
  | otherwise             = ioError e

-- | Starting and running the server.
main :: IO ()
main =
  do config <- mkConfig "bb4gg" >>= fetch
     createMissingDirectories config
     let dataPath = config ^. dirsBC . dataDC
     instanceNames <-
       fmap lines $
         readFile (instanceNamesDataPath dataPath)
           `catchIOError` instanceNamesReadHandlerWith dataPath
     instanceStates <-
       M.fromList . catMaybes <$>
         mapM
           ( \ name ->
               do let fileName = instanceDataPath dataPath name
                  file <-
                    BS.readFile fileName `catchIOError` instanceReadHandler
                  case runGet safeGet file
                    of Left err ->
                         Nothing <$ hPutStrLn stderr
                           ( "Error while reading file \""
                           ++ fileName
                           ++ "\":"
                           ++ err
                           )
                       Right sis ->
                         return $ Just (name, fromSavableInstanceState sis)
           )
           instanceNames
     instances <- M.traverseWithKey (initInstance dataPath) instanceStates
     bggState <- initBGG $ config ^. bggBC
     heistState <- initBB4GGHeist $ config ^. dirsBC . bb4ggDC </> "templates"
     bb4gg <- initBB4GG config instances
     let run :: forall c m a . BB4GGT c m a -> m a
         run action = runBB4GGT action bggState heistState bb4gg
     -- Cache all BGG items that occur in the instances
     run @ MonadIO
       ( void
       . retrieveBGGThings
       $ setOf (traverse . gamesIS . traverse . bggIDGD . traverse)
           instanceStates
       )
     -- Each hour, check for players and assignments to auto deactivate
     periodicAction 3600 $ run @ MonadIO autoDeactivate
     -- BEGIN sample instance hack
     -- This should probably be moved to a separate binary at some point.
     let argsMatch ("add-sample-instance":_) = True
         argsMatch _ = False
     whenM (argsMatch <$> getArgs) $ run @ MonadIO (addSampleInstance 8)
     -- END sample instance hack
     simpleHttpServe (config ^. snapBC) $
       (run :: BB4GGT MonadSnap _ _ -> _ _) site
