FROM haskell:8.8.3

RUN apt-get -y update \
    && apt-get -y -q install libglpk-dev \
    && rm -rf /var/lib/apt/lists/*

ENV rootdir=/code
WORKDIR $rootdir
ADD stack.yaml $rootdir/stack.yaml
ADD stack.yaml.lock $rootdir/stack.yaml.lock
ADD package.yaml $rootdir/package.yaml

RUN stack build --only-dependencies -j1

RUN mkdir log

ADD . $rootdir
RUN stack install

CMD bb4gg
